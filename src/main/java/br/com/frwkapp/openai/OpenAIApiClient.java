package br.com.frwkapp.openai;

import br.com.frwkapp.configuration.LoggingInterceptor;
import br.com.frwkapp.opencep.OpenCepClientInterface;
import br.com.frwkapp.opencep.dto.OpenCepDTO;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OpenAIApiClient {

    OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new LoggingInterceptor())
            .connectTimeout(Duration.ofMinutes(100l))
            .readTimeout(Duration.ofMinutes(2000l))
            .build();
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.openai.com/v1/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    OpenAIApiClientInterface service = retrofit.create(OpenAIApiClientInterface.class);

    public String prompt(String text) {
        Map<String, Object> message = new HashMap<>();
        message.put("role", "user");
        message.put("content", text);
        List<Map<String, Object>> messages = new ArrayList<>();
        messages.add(message);

        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("model", "gpt-4o");
        requestBody.put("messages", messages);
        requestBody.put("temperature", 0.7);

        Call<Map<String, Object>> call = service.prompt(requestBody);
        try {
            Response<Map<String, Object>> response = call.execute();
            if (response.isSuccessful() && response.body() != null) {
                Map<String, Object> responseBody = response.body();
                Map<String, Object> choice = (Map<String, Object>) ((List<?>) responseBody.get("choices")).get(0);
                Map<String, Object> messageResponse = (Map<String, Object>) choice.get("message");
                return (String) messageResponse.get("content");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Double> getEmbedding(String text) {
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("model", "text-embedding-ada-002");
        requestBody.put("input", text);

        Call<Map<String, Object>> call = service.getEmbedding(requestBody);
        try {
            Response<Map<String, Object>> response = call.execute();
            if (response.isSuccessful() && response.body() != null) {
                Map<String, Object> responseBody = response.body();
                Map<String, Object> data = (Map<String, Object>) ((List<?>) responseBody.get("data")).get(0);
                return (List<Double>) data.get("embedding");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

