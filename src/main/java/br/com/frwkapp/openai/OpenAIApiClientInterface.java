package br.com.frwkapp.openai;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

import java.util.Map;

public interface OpenAIApiClientInterface {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer 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"
    })
    @POST("chat/completions")
    Call<Map<String, Object>> prompt(@Body Map<String, Object> requestBody);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer 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"
    })
    @POST("embeddings")
    Call<Map<String, Object>> getEmbedding(@Body Map<String, Object> requestBody);
}
