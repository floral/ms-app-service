package br.com.frwkapp.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class DateUtil {
	public static final String DATE_DMY_HM_PATTERN = "dd-MM-yyyy HH:mm";
	public static final String DATE_YMD_HMS_PATTERN = "yyyy-MM-dd HH:mm:ss";

	private static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

	public static Date parseStandardDate(String date) throws ParseException {
		try {
			sdf = new SimpleDateFormat(DATE_DMY_HM_PATTERN);
			return sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static Date parseMinutesToDate(String minutes) {
		try {
			return sdf.parse(minutes);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String formatDateToMinutes(Date date) {
		return sdf.format(date);
	}

	public static String formatDate(Date date, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}

	public static String fromatDateToDB(Date date) {
		return formatDate(date, DATE_YMD_HMS_PATTERN);
	}

	public static String getEleapsedTime(Date dateInit, Date dateEnd) {

		long diff = dateInit.getTime() - dateEnd.getTime();

		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);

		return (diffDays != 0 ? Long.toString(diffDays) + "d " : "")
				+ (diffHours != 0 ? StringUtils.leftPad(Long.toString(diffHours), 2, '0') + "h " : "")
				+ (diffMinutes != 0 ? StringUtils.leftPad(Long.toString(diffMinutes), 2, '0') + "m " : "")
				+ (diffSeconds != 0 ? StringUtils.leftPad(Long.toString(diffSeconds), 2, '0') + "s " : "");

	}
}