package br.com.frwkapp.security;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.frwkapp.model.domain.UserAuth;
import br.com.frwkapp.model.domain.enums.RoleUser;
import br.com.frwkapp.model.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService {


	@Autowired
    private UserRepository repository;
	
	public LoginResultDTO createTokenForUser(UserAuth user) {

		LoginResultDTO dto = new LoginResultDTO();
		dto.setUserId(user.getId());
		dto.setUserName(user.getName());
		dto.setLogin(user.getLogin());
		
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.YEAR, 1);
		String token = Jwts.builder()
            .claim("id", user.getId())
            .claim("roles", user.getRoles())
            .setSubject(user.getLogin())
            .setExpiration(calendar.getTime())
            .signWith(SignatureAlgorithm.HS512, SecurityFilter.SECRET)
            .compact();
        dto.setToken(token);
        
        Calendar calendarRefresh = Calendar.getInstance();
        calendarRefresh.setTime(new Date());
        calendarRefresh.add(Calendar.YEAR, 1);
        
        String refreshToken = Jwts.builder()
                .claim("id", user.getId())
                .setSubject(user.getLogin())
                .setExpiration(calendarRefresh.getTime())
                .signWith(SignatureAlgorithm.HS512, SecurityFilter.SECRET)
                .compact();
        dto.setRefreshToken(CriptexFrw.encrypt(refreshToken));
        
        return dto;
	}
	
	public LoginResultDTO refreshToken(String refreshToken) {
		refreshToken = refreshToken.replace("Bearer ", "");
		refreshToken = CriptexFrw.decrypt(refreshToken);
		Claims claim = Jwts.parser().setSigningKey(SecurityFilter.SECRET).parseClaimsJws(refreshToken).getBody();
		UserAuth usuario = repository.getOne(Long.valueOf(claim.get("id", Integer.class).toString()));
		return createTokenForUser(usuario);
	}

	public UserJWT getUserJWTFromToken(String token) {
		token = token.replace("Bearer ", "");
	    token = CriptexFrw.decrypt(token);
		Claims claim = Jwts.parser().setSigningKey(SecurityFilter.SECRET).parseClaimsJws(token).getBody();
		return new UserJWT(claim.get("id", Long.class), claim.getSubject());
	}

	public boolean isValidToken(String token) {
		try {
            token = CriptexFrw.decrypt(token);
			Jwts.parser().setSigningKey(SecurityFilter.SECRET).parseClaimsJws(token);
			return true;
		} catch (ExpiredJwtException e) {
			return false;
		}
	}

//	private TipoUsuarioEnum convertStringToRoleAccess(String roleString) {
//		return TipoUsuarioEnum.valueOf(roleString);
//	}
}
