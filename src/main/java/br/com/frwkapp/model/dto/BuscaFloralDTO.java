package br.com.frwkapp.model.dto;

import java.util.List;

import lombok.Data;

@Data
public class BuscaFloralDTO {

	private List<Long> superar;
    private List<Long> alcancar;
}
