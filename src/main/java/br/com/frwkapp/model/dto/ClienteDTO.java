package br.com.frwkapp.model.dto;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.Cartao;
import br.com.frwkapp.model.domain.Cliente;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ClienteDTO extends BaseDTO {

    private Long id;
    private String idPagarme;
    private String nomeCompleto;
    private String cpf;
    private Date dataNascimento;
    private String numeroCelular;
    private UserDTO usuario;

    public ClienteDTO(Cliente cliente) {
    	this.id = cliente.getId();
    	this.idPagarme = cliente.getIdPagarme();
        this.nomeCompleto = cliente.getNomeCompleto();
        this.cpf = cliente.getCpf();
        this.dataNascimento = cliente.getDataNascimento();
        this.numeroCelular = cliente.getNumeroCelular();
        if (cliente.getUserAuth() != null) {
            this.usuario = new UserDTO(cliente.getUserAuth());
        }
    }
}
