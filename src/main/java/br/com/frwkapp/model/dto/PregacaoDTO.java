package br.com.frwkapp.model.dto;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.Pregacao;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PregacaoDTO extends BaseDTO {

    private String titulo;
    @SerializedName("referencias_biblicas")
    private String referenciasBiblicas;
    private String introducao;
    @SerializedName("mensagem_central")
    private String mensagemCentral;
    private String aplicacao;
    private String conclusao;
    private String oracao;
    private String resumo;
    @SerializedName("texto_completo_transcrito")
    private String transcricao;

    public PregacaoDTO(Pregacao pregacao) {
    	this.id = pregacao.getId();
    	this.titulo = pregacao.getTitulo();
    	this.referenciasBiblicas = pregacao.getReferenciasBiblicas();
        this.introducao = pregacao.getIntroducao();
        this.mensagemCentral = pregacao.getMensagemCentral();
        this.aplicacao = pregacao.getAplicacao();
        this.conclusao = pregacao.getConclusao();
        this.oracao = pregacao.getOracao();
        this.resumo = pregacao.getResumo();
        this.transcricao = pregacao.getTranscricao();
    }
}
