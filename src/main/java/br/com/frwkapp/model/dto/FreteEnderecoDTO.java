package br.com.frwkapp.model.dto;

import br.com.frwkapp.opencep.dto.OpenCepDTO;
import br.com.frwkapp.superfrete.dto.CotacaoFreteResponseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class FreteEnderecoDTO {

    private List<CotacaoFreteResponseDTO> opcoesFrete;
    private OpenCepDTO endereco;

}
