package br.com.frwkapp.model.dto;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.Cartao;
import br.com.frwkapp.model.domain.Endereco;
import br.com.frwkapp.pagarme.dto.CartaoPagarmeDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CartaoDTO extends BaseDTO {

    private Long id;
    private String idPagarme;
    private String nome;
    private String titular;
    private String idEnderecoPagarme;
    private String numero;
    private String ultimosQuatroNumeros;
    private String cpf;
    private int mes;
    private int ano;
    private String cvv;
    private String bandeira;

    public CartaoDTO(Cartao cartao) {
    	this.id = cartao.getId();
    	this.idPagarme = cartao.getIdPagarme();
        this.nome = cartao.getNome();
        this.ultimosQuatroNumeros = cartao.getUltimosQuatroNumeros();
        this.bandeira = cartao.getBandeira();
    }
}
