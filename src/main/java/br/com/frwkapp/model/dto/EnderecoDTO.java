package br.com.frwkapp.model.dto;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.Endereco;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EnderecoDTO extends BaseDTO {

    private Long id;
    private String idPagarme;
    private String nome;
    private String email;
    private String logradouro;
    private String numero;
    private String bairro;
    private String cep;
    private String complemento;
    private String cidade;
    private String uf;

    public EnderecoDTO(Endereco endereco) {
    	this.id = endereco.getId();
        this.idPagarme = endereco.getIdPagarme();
    	this.logradouro = endereco.getLogradouro();
        this.numero = endereco.getNumero();
        this.bairro = endereco.getBairro();
        this.cep = endereco.getCep();
        this.complemento = endereco.getComplemento();
        this.cidade = endereco.getCidade();
        this.uf = endereco.getUf();
    }
}
