package br.com.frwkapp.model.dto;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.Cliente;
import br.com.frwkapp.model.domain.CupomDesconto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CupomDescontoDTO extends BaseDTO {

    private Long id;
    private String nome;
    private String descricao;
    private Integer percentualDesconto;
    private Date dataValidade;
    private boolean ativo;

    public CupomDescontoDTO(CupomDesconto cupomDesconto) {
    	this.id = cupomDesconto.getId();
    	this.nome = cupomDesconto.getNome();
        this.descricao = cupomDesconto.getDescricao();
        this.percentualDesconto = cupomDesconto.getPercentualDesconto();
        this.dataValidade = cupomDesconto.getDataValidade();
        this.ativo = cupomDesconto.isAtivo();
    }
}
