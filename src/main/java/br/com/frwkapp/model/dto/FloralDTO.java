package br.com.frwkapp.model.dto;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.Floral;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class FloralDTO extends BaseDTO {

    private Long id;
    private String nome;
    private String indicacao;
    private String textoCompleto;
    private String exemplo;
    private String conclusao;
    private String textoPrompt;
    private RelacionamentosDTO relacionamentos;
    private String percentual;
    
    public FloralDTO(Floral floral) {
    	this.id = floral.getId();
    	this.nome = floral.getNome();
    	this.indicacao = floral.getIndicacao();
    	this.textoCompleto = floral.getTextoCompleto();
    	this.exemplo = floral.getExemplo();
    	this.conclusao = floral.getConclusao();
        this.textoPrompt = floral.getTextoGPT();
    }
}
