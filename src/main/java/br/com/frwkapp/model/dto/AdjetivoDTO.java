package br.com.frwkapp.model.dto;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.Adjetivo;
import br.com.frwkapp.model.domain.enums.ClassificacaoAdjetivoEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AdjetivoDTO extends BaseDTO {

    private Long id;
    private String nome;
    private ClassificacaoAdjetivoEnum classificacao;
    private String significado;
    private String colocacaoPositiva;
    private String colocacaoNegativa;
    private String colocacaoNeutra;
    private String pergunta;
    private String iconePositivo;
    private String iconeNegativo;
    private GrupoAdjetivoDTO grupo;
    
    public AdjetivoDTO(Adjetivo adjetivo) {
    	this.id = adjetivo.getId();
    	this.nome = adjetivo.getNome();
    	this.classificacao = adjetivo.getClassificacao();
        this.significado = adjetivo.getSignificado();
        this.colocacaoPositiva = adjetivo.getColocacaoPositiva();
        this.colocacaoNegativa = adjetivo.getColocacaoNegativa();
        this.colocacaoNeutra = adjetivo.getColocacaoNeutra();
        this.pergunta = adjetivo.getPergunta();
        this.iconePositivo = adjetivo.getIconePositivo();
        this.iconeNegativo = adjetivo.getIconeNegativo();
        if (adjetivo.getGrupo() != null) {
            this.grupo = new GrupoAdjetivoDTO(adjetivo.getGrupo());
        }
    }
}
