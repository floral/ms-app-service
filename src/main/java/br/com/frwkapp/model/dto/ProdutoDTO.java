package br.com.frwkapp.model.dto;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.Floral;
import br.com.frwkapp.model.domain.Produto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ProdutoDTO extends BaseDTO {

    private Long id;
    private String descricao;
    private String descricao2;
    private Double preco = 69.90;
    private Integer quantidade = 1;
    private String hash;
    private Integer percentualDesconto = 15;
    private List<FloralDTO> florais = new ArrayList<>();
    private List<MatchFloralDTO> matchFloralList = new ArrayList<>();

    public ProdutoDTO(Produto produto) {
    	this.id = produto.getId();
        this.descricao = produto.getDescricao();
        this.descricao2 = produto.getDescricao2();
        this.preco = produto.getPreco();
        this.quantidade = produto.getQuantidade();
        if (produto.getFlorais() != null) {
            for (Floral floral : produto.getFlorais()) {
                this.florais.add(new FloralDTO(floral));
            }
        }
    }
}
