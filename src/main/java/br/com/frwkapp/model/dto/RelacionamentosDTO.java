package br.com.frwkapp.model.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class RelacionamentosDTO {

	private Long floralId;
    private List<FloralAdjetivoDTO> adjetivosRelacionados = new ArrayList<>();
    
}
