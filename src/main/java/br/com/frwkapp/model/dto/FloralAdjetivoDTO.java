package br.com.frwkapp.model.dto;

import br.com.frwkapp.model.domain.enums.ObjetivoEnum;
import lombok.Data;

@Data
public class FloralAdjetivoDTO {

	private Long id;
	private Long floralId;
    private Long adjetivoId;
    private AdjetivoDTO adjetivoDTO;
    private ObjetivoEnum objetivo;
    
}
