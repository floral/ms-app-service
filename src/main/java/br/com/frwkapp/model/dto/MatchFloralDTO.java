package br.com.frwkapp.model.dto;

import lombok.Data;

@Data
public class MatchFloralDTO {
    private String nome;
    private String percentual;
}
