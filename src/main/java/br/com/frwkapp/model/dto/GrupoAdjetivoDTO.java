package br.com.frwkapp.model.dto;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.Adjetivo;
import br.com.frwkapp.model.domain.GrupoAdjetivo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class GrupoAdjetivoDTO extends BaseDTO {

    private Long id;
    private String nome;
    private String descricao;
    private List<AdjetivoDTO> adjetivos = new ArrayList<>();

    public GrupoAdjetivoDTO(GrupoAdjetivo grupo) {
    	this.id = grupo.getId();
    	this.nome = grupo.getNome();
        this.descricao = grupo.getDescricao();
    }
}
