package br.com.frwkapp.model.dto;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.Pedido;
import br.com.frwkapp.model.domain.enums.StatusPedidoEnum;
import br.com.frwkapp.model.domain.enums.TipoPagamentoEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PedidoDTO extends BaseDTO {

    private Long id;
    private ProdutoDTO produto;
    private EnderecoDTO endereco;
    private StatusPedidoEnum status;
    private CartaoDTO cartao;
    private ClienteDTO cliente;
    private TipoPagamentoEnum tipoPagamento;
    private Integer idFreteSelecionado;
    private double precoFreteSelecionado;
    private String nomeFreteSelecionado;
    private double valorTotal;
    private String qrcodeUrl;
    private String qrcodeCopiacola;
    private String cupom;
    private Date dataPedido;

    public PedidoDTO(Pedido pedido) {
    	this.id = pedido.getId();
        this.produto = new ProdutoDTO(pedido.getProduto());
        if (pedido.getEndereco() != null) {
            this.endereco = new EnderecoDTO(pedido.getEndereco());
        }
        this.status = pedido.getStatus();
        this.tipoPagamento = pedido.getTipoPagamento();
        if (pedido.getCartao() != null) {
            this.cartao = new CartaoDTO(pedido.getCartao());
        }
        this.precoFreteSelecionado = pedido.getPrecoFreteSelecionado();
        this.idFreteSelecionado = pedido.getIdFreteSelecionado();
        this.nomeFreteSelecionado = pedido.getNomeFreteSelecionado();
        this.qrcodeCopiacola = pedido.getQrcodeCopiacola();
        this.qrcodeUrl = pedido.getQrcodeUrl();
        this.valorTotal = pedido.getValorTotal();
        this.dataPedido = pedido.getCreatedAt();
        if (pedido.getCliente() != null) {
            this.cliente = new ClienteDTO(pedido.getCliente());
        }
    }
}