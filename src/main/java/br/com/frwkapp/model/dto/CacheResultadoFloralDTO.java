package br.com.frwkapp.model.dto;

import br.com.frwkapp.abstracts.BaseDTO;
import br.com.frwkapp.model.domain.CacheResultadoFloral;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CacheResultadoFloralDTO extends BaseDTO {

    private Long id;
    private String hash;
    private String texto;

    public CacheResultadoFloralDTO(CacheResultadoFloral endereco) {
    	this.id = endereco.getId();
        this.hash = endereco.getHash();
    	this.texto = endereco.getTexto();
    }
}
