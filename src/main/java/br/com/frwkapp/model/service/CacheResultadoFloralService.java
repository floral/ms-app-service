package br.com.frwkapp.model.service;

import br.com.frwkapp.abstracts.BaseMultiTenantService;
import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.CacheResultadoFloral;
import br.com.frwkapp.model.dto.CacheResultadoFloralDTO;
import br.com.frwkapp.model.repository.CacheResultadoFloralRepository;
import br.com.frwkapp.pagarme.PagarmeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CacheResultadoFloralService extends BaseMultiTenantService<CacheResultadoFloral, CacheResultadoFloralDTO> {

	@Autowired
	private CacheResultadoFloralRepository repository;

	private PagarmeClient pagarmeClient = new PagarmeClient();

	@Override
	public BaseRepository<CacheResultadoFloral> getRepository() {
		return repository;
	}

	@Override
	public CacheResultadoFloral parseDtoToEntity(CacheResultadoFloralDTO dto) {
		CacheResultadoFloral cache = new CacheResultadoFloral();
		cache.setId(dto.getId());
		cache.setHash(dto.getHash());
		cache.setTexto(dto.getTexto());
		return cache;
	}

	@Override
    public List<CacheResultadoFloralDTO> parseToDTO(List<CacheResultadoFloral> list) {
        return list.stream().map(CacheResultadoFloralDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<CacheResultadoFloralDTO> parseToDTO(Page<CacheResultadoFloral> page) {
        return page.map(CacheResultadoFloralDTO::new);
    }

    @Override
    public CacheResultadoFloralDTO parseToDTO(CacheResultadoFloral entity) {
        return new CacheResultadoFloralDTO(entity);
    }

	public CacheResultadoFloral buscaPorHash(String hash) {
		return repository.findOneByHash(hash);
	}
}
