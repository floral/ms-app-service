package br.com.frwkapp.model.service;

import br.com.frwkapp.abstracts.BaseMultiTenantService;
import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.*;
import br.com.frwkapp.model.domain.enums.StatusPedidoEnum;
import br.com.frwkapp.model.domain.enums.TipoPagamentoEnum;
import br.com.frwkapp.model.dto.FloralDTO;
import br.com.frwkapp.model.dto.FreteEnderecoDTO;
import br.com.frwkapp.model.dto.PedidoDTO;
import br.com.frwkapp.model.repository.FloralRepository;
import br.com.frwkapp.model.repository.PedidoRepository;
import br.com.frwkapp.opencep.OpenCepClient;
import br.com.frwkapp.opencep.dto.OpenCepDTO;
import br.com.frwkapp.pagarme.PagarmeClient;
import br.com.frwkapp.pagarme.dto.CartaoPagarmeDTO;
import br.com.frwkapp.pagarme.dto.EnderecoPagarmeDTO;
import br.com.frwkapp.pagarme.dto.PedidoPagarmeDTO;
import br.com.frwkapp.superfrete.SuperfreteClient;
import br.com.frwkapp.superfrete.dto.CotacaoFreteResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PedidoService extends BaseMultiTenantService<Pedido, PedidoDTO> {

	@Autowired
	private PedidoRepository repository;
	@Autowired
	private ProdutoService produtoService;
	@Autowired
	private EnderecoService enderecoService;
	@Autowired
	private CartaoService cartaoService;
	@Autowired
	private FloralRepository floralRepository;
	@Autowired
	private CupomDescontoService cupomDescontoService;
	@Autowired
	private ClienteService clienteService;

	private SuperfreteClient superFreteClient = new SuperfreteClient();
	private OpenCepClient openCepClient = new OpenCepClient();
	private PagarmeClient pagarmeClient = new PagarmeClient();

	@Override
	public BaseRepository<Pedido> getRepository() {
		return repository;
	}

	@Override
	public Pedido parseDtoToEntity(PedidoDTO dto) {
		Pedido pedido = new Pedido();
		pedido.setId(dto.getId());
		pedido.setProduto(produtoService.parseDtoToEntity(dto.getProduto()));
		pedido.setEndereco(enderecoService.parseDtoToEntity(dto.getEndereco()));
		pedido.setStatus(dto.getStatus());
		pedido.setIdFreteSelecionado(dto.getIdFreteSelecionado());
		pedido.setPrecoFreteSelecionado(dto.getPrecoFreteSelecionado());
		pedido.setNomeFreteSelecionado(dto.getNomeFreteSelecionado());
		pedido.setValorTotal(dto.getValorTotal());
		pedido.setTipoPagamento(dto.getTipoPagamento());
		return pedido;
	}

	public List<CotacaoFreteResponseDTO> calculaFrete(String cep) {
		return superFreteClient.calculaFrete(cep.replace("-", ""));
	}

	public OpenCepDTO buscaEnderecoPorCep(String cep) {
		return openCepClient.buscaEnderecoPorCEP(cep.replace("-", ""));
	}

	public FreteEnderecoDTO calculaFreteBuscaEndereco(String cep) {
		return new FreteEnderecoDTO(calculaFrete(cep), buscaEnderecoPorCep(cep));
	}

	public PedidoDTO checkout(PedidoDTO dto) {
		UserAuth userAuth = me();
		//List<CotacaoFreteResponseDTO> fretes = calculaFrete(dto.getEndereco().getCep());
		//CotacaoFreteResponseDTO cotacaoSelecionada = fretes.stream()
		//		.filter(cotacao -> cotacao.getId() == dto.getIdFreteSelecionado())
		//		.findFirst().get();
		//dto.setPrecoFreteSelecionado(cotacaoSelecionada.getPrice());
		//dto.setNomeFreteSelecionado(cotacaoSelecionada.getName());

		String[] floraisId = new String(Base64.getDecoder().decode(dto.getProduto().getHash())).split(",");
		List<FloralDTO> florais = new ArrayList<>();
		for (String id : floraisId) {
			florais.add(new FloralDTO(floralRepository.getOne(Long.valueOf(id))));
		}
		dto.getProduto().setFlorais(florais);
		dto.getProduto().setPreco(69.90);
		CupomDesconto cupomDesconto = null;
		if (dto.getCupom() != null) {
			cupomDesconto = cupomDescontoService.buscaPorNome(dto.getCupom());
		}
		if (dto.getProduto().getQuantidade() >= 3) {
			double totalSemDesconto = dto.getProduto().getQuantidade() * dto.getProduto().getPreco();
			double desconto = totalSemDesconto * (15d / 100d);
			dto.setValorTotal((totalSemDesconto - desconto));
		} else {
			dto.setValorTotal(dto.getProduto().getPreco() * dto.getProduto().getQuantidade());
		}
		if (cupomDesconto != null) {
			double desconto = dto.getValorTotal() * (cupomDesconto.getPercentualDesconto() / 100d);
			dto.setValorTotal(dto.getValorTotal() - desconto);
		}
		dto.setValorTotal(dto.getValorTotal() + dto.getPrecoFreteSelecionado());
		Pedido pedido = parseDtoToEntity(dto);
		//if (dto.getEndereco().getId() == null) {
		//	Endereco endereco = enderecoService.insert(dto.getEndereco());
		//	pedido.setEndereco(endereco);
		//} else {
		//	Endereco endereco = enderecoService.findOne(userAuth.getTenant(), dto.getEndereco().getId());
		//	pedido.setEndereco(endereco);
		//}
		/*if (TipoPagamentoEnum.CREDITO.equals(dto.getTipoPagamento())) {
			if (dto.getCartao().getId() == null) {
				if (dto.getCartao().getIdEnderecoPagarme() == null) {
					dto.getCartao().setIdEnderecoPagarme(pedido.getEndereco().getIdPagarme());
				}
				Cartao cartao = cartaoService.insert(dto.getCartao());
				pedido.setCartao(cartao);
			} else {
				Cartao cartao = cartaoService.findOne(userAuth.getTenant(), dto.getCartao().getId());
				pedido.setCartao(cartao);
			}
		}*/

		/*PedidoPagarmeDTO pedidoPagarmeDTO = new PedidoPagarmeDTO(userAuth.getIdClientePagarme(), dto);
		pedidoPagarmeDTO = pagarmeClient.criaPedido(pedidoPagarmeDTO);
		pedido.setIdPagarme(pedidoPagarmeDTO.getId());
		if (TipoPagamentoEnum.PIX.equals(pedido.getTipoPagamento())) {
			pedido.setQrcodeCopiacola(pedidoPagarmeDTO.getCharges().get(0).getLastTransaction().getQrCode());
			pedido.setQrcodeUrl(pedidoPagarmeDTO.getCharges().get(0).getLastTransaction().getQrCodeUrl());
		}*/
		Cliente cliente = clienteService.buscarPorUserAuth(userAuth);
		pedido.setCliente(cliente);
		pedido = insert(pedido);
		return parseToDTO(pedido);
	}

	@Override
	public Pedido insert(Pedido entity) {
		produtoService.insert(entity.getProduto());
		UserAuth userAuth = me();
		entity.setUserAuth(userAuth);
		entity.setTenant(userAuth.getTenant());
		entity.setStatus(StatusPedidoEnum.AGUARDANDO_PAGAMENTO);
		return super.insert(entity);
	}

	@Override
    public List<PedidoDTO> parseToDTO(List<Pedido> list) {
        return list.stream().map(PedidoDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<PedidoDTO> parseToDTO(Page<Pedido> page) {
        return page.map(PedidoDTO::new);
    }

    @Override
    public PedidoDTO parseToDTO(Pedido entity) {
        return new PedidoDTO(entity);
    }

	public Page<PedidoDTO> listPedidosAdmin(Pageable pageable) {
		return parseToDTO(repository.findAllByOrderByIdDesc(pageable));
	}

}
