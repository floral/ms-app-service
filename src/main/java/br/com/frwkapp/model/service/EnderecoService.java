package br.com.frwkapp.model.service;

import br.com.frwkapp.abstracts.BaseMultiTenantService;
import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Endereco;
import br.com.frwkapp.model.domain.UserAuth;
import br.com.frwkapp.model.dto.EnderecoDTO;
import br.com.frwkapp.model.repository.EnderecoRepository;
import br.com.frwkapp.pagarme.PagarmeClient;
import br.com.frwkapp.pagarme.dto.EnderecoPagarmeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EnderecoService extends BaseMultiTenantService<Endereco, EnderecoDTO> {

	@Autowired
	private EnderecoRepository repository;

	private PagarmeClient pagarmeClient = new PagarmeClient();

	@Override
	public BaseRepository<Endereco> getRepository() {
		return repository;
	}

	@Override
	public Endereco parseDtoToEntity(EnderecoDTO dto) {
		if (dto == null) return null;
		Endereco endereco = new Endereco();
		endereco.setId(dto.getId());
		endereco.setLogradouro(dto.getLogradouro());
		endereco.setNumero(dto.getNumero());
		endereco.setBairro(dto.getBairro());
		endereco.setCep(dto.getCep());
		endereco.setComplemento(dto.getComplemento());
		endereco.setCidade(dto.getCidade());
		endereco.setUf(dto.getUf());

		return endereco;
	}

	@Override
	public Endereco insert(Endereco entity) {
		UserAuth userAuth = me();
		EnderecoPagarmeDTO dto = new EnderecoPagarmeDTO();
		dto.setLine_1(entity.getLogradouro() + ", " + entity.getNumero());
		dto.setLine_2(entity.getComplemento());
		dto.setZip_code(entity.getCep());
		dto.setCity(entity.getCidade());
		dto.setState(entity.getUf());
		dto = pagarmeClient.cadastraEnderecoCliente(userAuth.getIdClientePagarme(), dto);
		entity.setIdPagarme(dto.getId());

		return super.insert(entity);
	}

	@Override
    public List<EnderecoDTO> parseToDTO(List<Endereco> list) {
        return list.stream().map(EnderecoDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<EnderecoDTO> parseToDTO(Page<Endereco> page) {
        return page.map(EnderecoDTO::new);
    }

    @Override
    public EnderecoDTO parseToDTO(Endereco entity) {
        return new EnderecoDTO(entity);
    }
}
