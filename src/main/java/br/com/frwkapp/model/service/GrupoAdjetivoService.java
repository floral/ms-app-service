package br.com.frwkapp.model.service;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Adjetivo;
import br.com.frwkapp.model.domain.GrupoAdjetivo;
import br.com.frwkapp.model.domain.Tenant;
import br.com.frwkapp.model.dto.GrupoAdjetivoDTO;
import br.com.frwkapp.model.repository.GrupoAdjetivoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GrupoAdjetivoService extends BaseService<GrupoAdjetivo, GrupoAdjetivoDTO> {

	@Autowired
	private GrupoAdjetivoRepository repository;
	@Autowired
	private AdjetivoService adjetivoService;


	@Override
	public BaseRepository<GrupoAdjetivo> getRepository() {
		return repository;
	}

	@Override
	public GrupoAdjetivo parseDtoToEntity(GrupoAdjetivoDTO dto) {
		GrupoAdjetivo entity = new GrupoAdjetivo();
		entity.setId(dto.getId());
		entity.setNome(dto.getNome());
		entity.setDescricao(dto.getDescricao());
		return entity;
	}
	
	@Override
    public List<GrupoAdjetivoDTO> parseToDTO(List<GrupoAdjetivo> list) {
		List<GrupoAdjetivoDTO> result = list.stream().map(GrupoAdjetivoDTO::new).collect(Collectors.toList());
		result.forEach(grupo -> {
			List<Adjetivo> adjetivos = adjetivoService.findAllByGrupo(grupo.getId());
			grupo.setAdjetivos(adjetivoService.parseToDTO(adjetivos));
		});
		return result;
    }

    @Override
    public Page<GrupoAdjetivoDTO> parseToDTO(Page<GrupoAdjetivo> page) {
        return page.map(GrupoAdjetivoDTO::new);
    }

    @Override
    public GrupoAdjetivoDTO parseToDTO(GrupoAdjetivo entity) {
		GrupoAdjetivoDTO grupo = new GrupoAdjetivoDTO(entity);
		List<Adjetivo> adjetivos = adjetivoService.findAllByGrupo(grupo.getId());
		grupo.setAdjetivos(adjetivoService.parseToDTO(adjetivos));
		return grupo;
    }

	@Override
	public List<GrupoAdjetivo> findAll(String search) {
		if (search == null) {
			return repository.findByOrderByNomeAsc();
		} else {
			return repository.findAllByIndexContainingIgnoreCaseOrderByNome(search);
		}
	}
}
