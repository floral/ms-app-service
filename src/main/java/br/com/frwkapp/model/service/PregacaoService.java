package br.com.frwkapp.model.service;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Adjetivo;
import br.com.frwkapp.model.domain.Floral;
import br.com.frwkapp.model.domain.Pregacao;
import br.com.frwkapp.model.domain.Pregacao;
import br.com.frwkapp.model.dto.PregacaoDTO;
import br.com.frwkapp.model.dto.PregacaoDTO;
import br.com.frwkapp.model.repository.PregacaoRepository;
import br.com.frwkapp.model.repository.PregacaoRepository;
import br.com.frwkapp.openai.OpenAIApiClient;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PregacaoService extends BaseService<Pregacao, PregacaoDTO> {

	@Autowired
	private PregacaoRepository repository;
	

	@Override
	public BaseRepository<Pregacao> getRepository() {
		return repository;
	}

	@Override
	public Pregacao parseDtoToEntity(PregacaoDTO dto) {
		Pregacao entity = new Pregacao();
		entity.setId(dto.getId());
		return entity;
	}
	
	@Override
    public List<PregacaoDTO> parseToDTO(List<Pregacao> list) {
		return list.stream().map(PregacaoDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<PregacaoDTO> parseToDTO(Page<Pregacao> page) {
        return page.map(PregacaoDTO::new);
    }

    @Override
    public PregacaoDTO parseToDTO(Pregacao entity) {
		return new PregacaoDTO(entity);
    }

	public void processaPregacoes() {
		OpenAIApiClient openAIApiClient = new OpenAIApiClient();
		List<Pregacao> pregacoes = repository.findAll();
		for (Pregacao pregacao : pregacoes) {
			try {
				if (pregacao.getTitulo() == "Processando pregação 8 - 01. Como tudo começou (At 1.1-5) (1080p_30fps_H264-128kbit_AAC).português.txt" || pregacao.getIntroducao() != null) {
					System.out.println("ja foi");
				} else {
					System.out.println("Processando pregação "+pregacao.getId() + " - "+pregacao.getTitulo());
					String resultado = openAIApiClient.prompt("o texto a seguir foi retirado do arquivo de legenda de um video de pregação do pastor Augustus Nicodemus e quero que retorne em json (sem acrescentar nenhum outro texto na resposta apenas o json) os seguintes atributos caso consiga encontrar no texto original (referencias_bilblicas, introducao, mensagem_central, aplicacao, conclusao, oracao, resumo, texto_completo_transcrito) considerando que no texto_completo_transcrito quero que transforme o texto original em um texto bem estruturado e sempre em uma string unica assim como o atributo referencias_biblicas deve ser uma string unica: "+pregacao.getPregacao());
					resultado = resultado.replace("```json\n", "").replace("```", "");
					System.out.println(resultado);
					PregacaoDTO dto = new Gson().fromJson(resultado, PregacaoDTO.class);
					pregacao.setIntroducao(dto.getIntroducao());
					pregacao.setMensagemCentral(dto.getMensagemCentral());
					pregacao.setAplicacao(dto.getAplicacao());
					pregacao.setConclusao(dto.getConclusao());
					pregacao.setOracao(dto.getOracao());
					pregacao.setResumo(dto.getResumo());
					pregacao.setTranscricao(dto.getTranscricao());
					pregacao.setReferenciasBiblicas(dto.getReferenciasBiblicas());

					repository.save(pregacao);
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}


		}
	}

	public String buildEmbeddings() {
		OpenAIApiClient openAIApiClient = new OpenAIApiClient();
		List<Pregacao> florais = repository.findAll();
		StringBuilder sql = new StringBuilder();
		for (Pregacao floral : florais) {
			if (floral.getIntroducao() != null) {
				String builder = floral.getIntroducao() + floral.getTitulo();
				List<Double> embeddingVector = openAIApiClient.getEmbedding(builder);
				sql.append("update t_pregacao set vetores = ").append(embeddingVector.toString()).append(" where id = ").append(floral.getId()).append(";");
			}
		}
		System.out.println(sql);
		return sql.toString();
	}

	// Mapa para armazenar os históricos de conversa por usuário
	private static Map<String, StringBuilder> historicosPorUsuario = new HashMap<>();

	// Método para gerar a resposta e gerenciar o histórico
	public String prompt(String promptUsuario, String idUsuario) {

		if (!historicosPorUsuario.containsKey(idUsuario)) {
			if (!promptUsuario.toLowerCase().equals("shock@2020")) {
				return "Não disponível";
			} else {
				historicosPorUsuario.putIfAbsent(idUsuario, new StringBuilder());
				return "Olá!";
			}
		}

		OpenAIApiClient openAIApiClient = new OpenAIApiClient();

		// Obtém o embedding da pergunta do usuário
		List<Double> embedding = openAIApiClient.getEmbedding(promptUsuario);
		String pregacao = repository.buscaPregacao(embedding.toString());

		// Inicializa o histórico do usuário, caso não exista ainda
		historicosPorUsuario.putIfAbsent(idUsuario, new StringBuilder());

		// Cria o prompt inicial
		StringBuilder promptGpt = new StringBuilder();
		StringBuilder historico = historicosPorUsuario.get(idUsuario); // Obtenha o histórico específico do usuário

		if (historico.length() == 0) {
			promptGpt.append("Você é Augustus Nicodemus, um renomado teólogo e pastor da igreja presbiteriana do Brasil. Responda às perguntas do usuário de forma pastoral e teológica, sem se apresentar novamente após a primeira saudação. Mantenha as respostas objetivas e relevantes para a pergunta do usuário.");
		} else {
			promptGpt.append("Você é Augustus Nicodemus, pastor da igreja presbiteriana do Brasil. Responda diretamente à pergunta abaixo sem se apresentar novamente.");
		}

		// Inclui a pregação, se houver uma relevante
		promptGpt.append(" Responda abaixo");
		if (pregacao != null && !pregacao.isEmpty()) {
			promptGpt.append(" e, se achar (somente se tiver certeza) que há conexão com a pergunta do usuário, faça referência à seguinte pregação e considere que o usuário não a viu então não use 'com a pregação mencionada' na reposta mas traga os trechos da pregação que achar relevante: ").append(pregacao);		} else {
			promptGpt.append(".");
		}

		// Adiciona a pergunta do usuário
		promptGpt.append(" Pergunta do usuário: ").append(promptUsuario);

		// Inclui o histórico se houver
		if (historico.length() > 0) {
			promptGpt.append(" Histórico: ").append(historico);
		}

		// Faz a chamada para o OpenAI
		String resposta = openAIApiClient.prompt(promptGpt.toString());

		// Atualiza o histórico do usuário com a nova interação
		historico.append("\nUsuário: ").append(promptUsuario).append("\nAugustus: ").append(resposta);

		// Limita o tamanho do histórico (opcional: limite de interações)
		limitarTamanhoDoHistorico(idUsuario, 10); // Exemplo, mantém as últimas 10 interações.

		return resposta;
	}

	// Método opcional para limitar o tamanho do histórico por usuário
	private void limitarTamanhoDoHistorico(String idUsuario, int limiteInteracoes) {
		StringBuilder historico = historicosPorUsuario.get(idUsuario);
		String[] interacoes = historico.toString().split("\n");
		if (interacoes.length > limiteInteracoes * 2) { // 2 linhas por interação (Usuário + Augustus)
			historico.setLength(0); // Limpa o histórico atual
			historico.append(String.join("\n", Arrays.copyOfRange(interacoes, interacoes.length - (limiteInteracoes * 2), interacoes.length))); // Mantém apenas as últimas interações
		}
	}

}
