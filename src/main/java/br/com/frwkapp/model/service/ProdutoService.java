package br.com.frwkapp.model.service;

import br.com.frwkapp.abstracts.BaseMultiTenantService;
import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Produto;
import br.com.frwkapp.model.domain.Floral;
import br.com.frwkapp.model.dto.ProdutoDTO;
import br.com.frwkapp.model.dto.FloralDTO;
import br.com.frwkapp.model.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProdutoService extends BaseMultiTenantService<Produto, ProdutoDTO> {

	@Autowired
	private ProdutoRepository repository;


	@Override
	public BaseRepository<Produto> getRepository() {
		return repository;
	}

	@Override
	public Produto parseDtoToEntity(ProdutoDTO dto) {
		Produto produto = new Produto();
		produto.setId(dto.getId());
		produto.setDescricao(dto.getDescricao());
		produto.setPreco(dto.getPreco());
		produto.setQuantidade(dto.getQuantidade());
		if (dto.getFlorais() != null && !dto.getFlorais().isEmpty()) {
			for (FloralDTO floral : dto.getFlorais()) {
				produto.getFlorais().add(new Floral(floral.getId()));
			}
		}

		return produto;
	}
	
	@Override
    public List<ProdutoDTO> parseToDTO(List<Produto> list) {
        return list.stream().map(ProdutoDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<ProdutoDTO> parseToDTO(Page<Produto> page) {
        return page.map(ProdutoDTO::new);
    }

    @Override
    public ProdutoDTO parseToDTO(Produto entity) {
        return new ProdutoDTO(entity);
    }
}
