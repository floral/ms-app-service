package br.com.frwkapp.model.service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;


import br.com.frwkapp.model.domain.CacheResultadoFloral;
import br.com.frwkapp.model.dto.*;
import br.com.frwkapp.openai.OpenAIApiClient;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Adjetivo;
import br.com.frwkapp.model.domain.Floral;
import br.com.frwkapp.model.domain.FloralAdjetivo;
import br.com.frwkapp.model.domain.enums.ObjetivoEnum;
import br.com.frwkapp.model.repository.FloralAdjetivoRepository;
import br.com.frwkapp.model.repository.FloralRepository;

@Service
public class FloralService extends BaseService<Floral, FloralDTO> {

	@Autowired
	private FloralRepository repository;
	@Autowired
	private FloralAdjetivoRepository floralAdjetivoRepository;
	@Autowired
	private AdjetivoService adjetivoService;
	@Autowired
	private CacheResultadoFloralService cacheResultadoFloralService;

	private OpenAIApiClient openAIApiClient = new OpenAIApiClient();

	@Override
	public BaseRepository<Floral> getRepository() {
		return repository;
	}

	@Override
	public Floral parseDtoToEntity(FloralDTO dto) {
		Floral entity = new Floral();
		entity.setId(dto.getId());
		entity.setNome(dto.getNome());
		entity.setIndicacao(dto.getIndicacao());
		entity.setTextoCompleto(dto.getTextoCompleto());
		entity.setExemplo(dto.getExemplo());
		entity.setConclusao(dto.getConclusao());
		return entity;
	}

	@Override
    public List<FloralDTO> parseToDTO(List<Floral> list) {
        return list.stream().map(FloralDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<FloralDTO> parseToDTO(Page<Floral> page) {
        return page.map(FloralDTO::new);
    }

    @Override
    public FloralDTO parseToDTO(Floral entity) {
        return new FloralDTO(entity);
    }

	@Override
	public List<Floral> findAll(String search) {
		if (search == null) {
			return repository.findByOrderByNomeAsc();
		} else {
			return repository.findAllByIndexContainingIgnoreCaseOrderByNome(search);
		}
	}

	public List<MatchFloralDTO> buscaFloralMaisIndicada(List<Long> superar, List<Long> alcancar) {
		StringBuilder afirmacoes = new StringBuilder();
		for (Long id : superar) {
			Adjetivo adjetivo = adjetivoService.findOne(id);
			afirmacoes.append(" - ");
			afirmacoes.append(adjetivo.getColocacaoNegativa());
		}
		String resultado = openAIApiClient.prompt("Você é um especialista em Florais de Bach. Analise as afirmações a seguir e selecione 4 essências florais de Bach que podem ajudar esta pessoa. Para cada essência, calcule um percentual de correspondência (evitando percentuais iguais) com base nas afirmações fornecidas. Responda apenas com o resultado final em formato JSON array, como se fosse uma API de backend, onde cada objeto contém os atributos 'nome' e 'percentual' sem quebras de linha. Exemplo: [{'nome':'Essência 1','percentual':40},{'nome':'Essência 2','percentual':30},{'nome':'Essência 3','percentual':20},{'nome':'Essência 4','percentual':10}]. Aqui estão as afirmações: "+afirmacoes);
		resultado = resultado.replace("```json", "");
		resultado = resultado.replace("```", "");
		Gson gson = new Gson();
		Type matchFloralListType = new TypeToken<List<MatchFloralDTO>>(){}.getType();
		List<MatchFloralDTO> matchFloralList = gson.fromJson(resultado, matchFloralListType);
		return matchFloralList;

//		String result = "";
//		if ( superar.isEmpty()) {
//			result = repository.buscaFloralMaisIndicadaAlcancar(alcancar);
//		} else if (alcancar.isEmpty()) {
//			result = repository.buscaFloralMaisIndicadaSuperar(superar);
//		} else {
//			result = repository.buscaFloralMaisIndicada(superar, alcancar);
//		}
//
//    	String[] ids = result.split(",");
//    	List<FloralDTO> florais = new ArrayList<>();
//    	for (String id : ids) {
//    		Floral floral = repository.getOne(Long.valueOf(id));
//    		florais.add(parseToDTO(floral));
//    	}

//    	return florais;
    }

	public List<FloralDTO> buscaFloralMaisIndicadaAI(String busca) {
		List<Double> vector = openAIApiClient.getEmbedding(busca);
		String result = repository.buscaFloralMaisIndicadaAI(vector.toString());
		String[] ids = result.split(",");
		List<FloralDTO> florais = new ArrayList<>();
		for (String id : ids) {
			Floral floral = repository.getOne(Long.valueOf(id));
			florais.add(parseToDTO(floral));
		}

		return florais;
	}

	public ProdutoDTO formulaProdutoAI(String busca) {
		List<FloralDTO> florais = buscaFloralMaisIndicadaAI(busca);
		return formulaProduto(florais, null, null);
	}

	public ProdutoDTO formulaProdutoQuestionario(List<Long> superar, List<Long> alcancar) {
		List<MatchFloralDTO> matchFloralList = buscaFloralMaisIndicada(superar, new ArrayList<>());
		List<FloralDTO> florais = new ArrayList<>();
		for (MatchFloralDTO f : matchFloralList) {
			Floral floral = repository.findFirstByNomeContainingIgnoreCase(f.getNome().toLowerCase());
			FloralDTO dto = parseToDTO(floral);
			dto.setPercentual(f.getPercentual());
			florais.add(dto);
		}
		List<Adjetivo> adjetivos = adjetivoService.findAllByIds(superar);
		return formulaProduto(florais, adjetivos, matchFloralList);
	}

	private ProdutoDTO formulaProduto(List<FloralDTO> florais, List<Adjetivo> adjetivos, List<MatchFloralDTO> matchFloralList) {
		StringBuilder prompt1Builder = new StringBuilder("Acabei de responder um questionário e o texto abaixo é o resultado que irá definir qual será meu processo terapêutico com florais de Bach que vou seguir então faça para mim um resumo do texto contendo o máximo de 250 palavras e mesclando tudo em uma única personalidade e ao final crie um texto inspirador sobre como esse tratamento terapêutico Petália pode me ajudar e formule o texto de forma que fale diretamente comigo: ");
		florais.forEach(floral -> prompt1Builder.append(floral.getTextoPrompt()).append(" "));
		String prompt1 = prompt1Builder.toString();
		String hash = florais.stream()
				.map(floral -> String.valueOf(floral.getId()))
				.collect(Collectors.joining(","));
		hash = Base64.getEncoder().encodeToString(hash.getBytes());
		CacheResultadoFloral cache = cacheResultadoFloralService.buscaPorHash(hash);
		if (cache == null) {
			cache = new CacheResultadoFloral();
			cache.setHash(hash);
			cache.setTexto(openAIApiClient.prompt(prompt1));
			cache.setTexto2(new Gson().toJson(matchFloralList));
			cacheResultadoFloralService.insert(cache);
		}
		ProdutoDTO produto = new ProdutoDTO();
		produto.setDescricao(cache.getTexto());
		produto.setMatchFloralList(matchFloralList);
		produto.setFlorais(florais);
		produto.setHash(hash);
		return produto;
	}


	public RelacionamentosDTO buscaRelacionamentos(Long floralId) {
    	Floral floral = repository.getOne(floralId);
    	RelacionamentosDTO relacionamentos = new RelacionamentosDTO();
		relacionamentos.setFloralId(floral.getId());
    	List<FloralAdjetivo> list = floralAdjetivoRepository.findAllByFloralAndObjetivo(floral, ObjetivoEnum.ALCANCAR);
    	list.forEach(i -> {
    		FloralAdjetivoDTO dto = new FloralAdjetivoDTO();
    		dto.setId(i.getId());
    		dto.setAdjetivoId(i.getAdjetivo().getId());
    		dto.setAdjetivoDTO(adjetivoService.parseToDTO(i.getAdjetivo()));
    		dto.setFloralId(i.getFloral().getId());
    		dto.setObjetivo(i.getObjetivo());
    		relacionamentos.getAdjetivosRelacionados().add(dto);
    	});

    	list = floralAdjetivoRepository.findAllByFloralAndObjetivo(floral, ObjetivoEnum.SUPERAR);
    	list.forEach(i -> {
    		FloralAdjetivoDTO dto = new FloralAdjetivoDTO();
    		dto.setId(i.getId());
    		dto.setAdjetivoId(i.getAdjetivo().getId());
    		dto.setAdjetivoDTO(adjetivoService.parseToDTO(i.getAdjetivo()));
    		dto.setFloralId(i.getFloral().getId());
    		dto.setObjetivo(i.getObjetivo());
    		relacionamentos.getAdjetivosRelacionados().add(dto);
    	});

    	return relacionamentos;
    }

    public boolean salvaRelacionamento(Long floral, Long adjetivo, ObjetivoEnum objetivo) {
    	FloralAdjetivo relacionamento = new FloralAdjetivo();
    	relacionamento.setFloral(new Floral(floral));
    	relacionamento.setAdjetivo(new Adjetivo(adjetivo));
    	relacionamento.setObjetivo(objetivo);
    	floralAdjetivoRepository.save(relacionamento);
    	return true;
    }

    public boolean atualizaRelacionamento(Long id, Long floral, Long adjetivo, ObjetivoEnum objetivo) {
    	FloralAdjetivo relacionamento = floralAdjetivoRepository.getOne(id);
    	if (relacionamento != null) {
    		relacionamento.setFloral(new Floral(floral));
        	relacionamento.setAdjetivo(new Adjetivo(adjetivo));
    		relacionamento.setObjetivo(objetivo);
    		floralAdjetivoRepository.save(relacionamento);
    		return true;
    	}
    	return false;
    }

    public boolean removeRelacionamento(Long id) {
    	floralAdjetivoRepository.deleteById(id);
    	return true;
    }

	public String buildEmbeddings() {
		List<Floral> florais = repository.findAll();
		StringBuilder sql = new StringBuilder();
		for (Floral floral : florais) {
			String builder = floral.getNome() +
					floral.getTextoCompleto() +
					floral.getExemplo() +
					floral.getIndicacao() +
					floral.getConclusao();
			List<Double> embeddingVector = openAIApiClient.getEmbedding(builder);
			sql.append("update t_floral set embedding = ").append(embeddingVector.toString()).append(" where id = ").append(floral.getId()).append(";");
		}
		return sql.toString();
	}
}