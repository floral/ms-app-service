package br.com.frwkapp.model.service;

import br.com.frwkapp.abstracts.BaseMultiTenantService;
import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.Cliente;
import br.com.frwkapp.model.domain.UserAuth;
import br.com.frwkapp.model.dto.ClienteDTO;
import br.com.frwkapp.model.dto.PedidoDTO;
import br.com.frwkapp.model.repository.ClienteRepository;
import br.com.frwkapp.pagarme.PagarmeClient;
import br.com.frwkapp.pagarme.dto.ClientePagarmeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClienteService extends BaseMultiTenantService<Cliente, ClienteDTO> {

	@Autowired
	private ClienteRepository repository;

	@Override
	public BaseRepository<Cliente> getRepository() {
		return repository;
	}

	@Override
	public Cliente parseDtoToEntity(ClienteDTO dto) {
		Cliente cliente = new Cliente();
		cliente.setId(dto.getId());
		cliente.setIdPagarme(dto.getIdPagarme());
		cliente.setNomeCompleto(dto.getNomeCompleto());
		cliente.setCpf(dto.getCpf());
		cliente.setDataNascimento(dto.getDataNascimento());
		cliente.setNumeroCelular(dto.getNumeroCelular());
		return cliente;
	}

	@Override
    public List<ClienteDTO> parseToDTO(List<Cliente> list) {
        return list.stream().map(ClienteDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<ClienteDTO> parseToDTO(Page<Cliente> page) {
        return page.map(ClienteDTO::new);
    }

    @Override
    public ClienteDTO parseToDTO(Cliente entity) {
        return new ClienteDTO(entity);
    }

	public Cliente buscarPorUserAuth(UserAuth userAuth) {
		return repository.findOneByUserAuth(userAuth);
	}

	public Page<ClienteDTO> listClientesAdmin(Pageable pageable) {
		return parseToDTO(repository.findAllByOrderByIdDesc(pageable));
	}
}
