package br.com.frwkapp.model.service;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.CupomDesconto;
import br.com.frwkapp.model.dto.CupomDescontoDTO;
import br.com.frwkapp.model.repository.CupomDescontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CupomDescontoService extends BaseService<CupomDesconto, CupomDescontoDTO> {

	@Autowired
	private CupomDescontoRepository repository;

	@Override
	public BaseRepository<CupomDesconto> getRepository() {
		return repository;
	}

	@Override
	public CupomDesconto parseDtoToEntity(CupomDescontoDTO dto) {
		CupomDesconto cupomDesconto = new CupomDesconto();
		cupomDesconto.setId(dto.getId());
		cupomDesconto.setNome(dto.getNome());
		cupomDesconto.setDescricao(dto.getDescricao());
		cupomDesconto.setPercentualDesconto(dto.getPercentualDesconto());
		cupomDesconto.setDataValidade(dto.getDataValidade());
		cupomDesconto.setAtivo(dto.isAtivo());
		return cupomDesconto;
	}

	@Override
    public List<CupomDescontoDTO> parseToDTO(List<CupomDesconto> list) {
        return list.stream().map(CupomDescontoDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<CupomDescontoDTO> parseToDTO(Page<CupomDesconto> page) {
        return page.map(CupomDescontoDTO::new);
    }

    @Override
    public CupomDescontoDTO parseToDTO(CupomDesconto entity) {
        return new CupomDescontoDTO(entity);
    }

	public CupomDesconto buscaPorNome(String nome) {
		CupomDesconto cupomDesconto = repository.findFirstByNomeAndAtivoIsTrue(nome);
		if (cupomDesconto != null) {
			Date currentDate = new Date();
			if (cupomDesconto.getDataValidade().before(currentDate) || !cupomDesconto.isAtivo()) {
				return null;
			}
		}
		return cupomDesconto;
	}
}
