package br.com.frwkapp.model.service;

import java.util.*;
import java.util.stream.Collectors;

import br.com.frwkapp.abstracts.BaseMultiTenantService;
import br.com.frwkapp.abstracts.EmailService;
import br.com.frwkapp.model.domain.Tenant;
import br.com.frwkapp.model.exception.UnauthorizedException;
import br.com.frwkapp.model.repository.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.UserAuth;
import br.com.frwkapp.model.domain.enums.RoleUser;
import br.com.frwkapp.model.dto.UserDTO;
import br.com.frwkapp.model.exception.UserDisabledException;
import br.com.frwkapp.model.exception.UserDuplicatedException;
import br.com.frwkapp.model.repository.UserRepository;
import br.com.frwkapp.security.LoginResultDTO;
import br.com.frwkapp.security.TokenService;


@Service
public class UserService extends BaseMultiTenantService<UserAuth, UserDTO> {

	@Autowired
	private UserRepository repository;

	@Autowired
	private TenantRepository tenantRepository;

	@Autowired
	private TokenService tokenService;

	@Autowired
	private EmailService emailService;

	@Override
	public BaseRepository<UserAuth> getRepository() {
		return repository;
	}

	@Override
	public UserAuth parseDtoToEntity(UserDTO dto) {
		UserAuth entity = new UserAuth();
		entity.setId(dto.getId());
		entity.setLogin(dto.getLogin());
		entity.setPassword(dto.getPassword());
		entity.setName(dto.getName());
		if (dto.getRoles() != null && dto.getRoles().size() > 0)
			dto.getRoles().forEach(role -> { entity.getRoles().add(RoleUser.getEnum(role)); });
		return entity;
	}
	
	@Override
    public List<UserDTO> parseToDTO(List<UserAuth> list) {
        return list.stream().map(UserDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<UserDTO> parseToDTO(Page<UserAuth> page) {
        return page.map(UserDTO::new);
    }

    @Override
    public UserDTO parseToDTO(UserAuth entity) {
        return new UserDTO(entity);
    }
    
    public UserDTO parseToDTOAuth(UserAuth entity) {
       UserDTO user = new UserDTO(entity);
        if (entity.getRoles() != null && entity.getRoles().size() > 0) {
        	user.setRoles(new ArrayList<>());
        	entity.getRoles().forEach(role -> { user.getRoles().add(role.name()); });
        }
		return user;
    }

	public LoginResultDTO login(String login, String pass, String tokenFirebase) throws Exception {
		
		UserAuth user = repository.findByLoginAndPassword(login, pass);
		if (repository.findByLogin(login) != null) {
			user = repository.findByLogin(login);
			user.setPassword(pass);
			user.setUserUpdate(user.getName());
		}
		if (user.isDeleted()) {
			throw new UserDisabledException();
		}
		
		repository.save(user);
		
		LoginResultDTO dto = tokenService.createTokenForUser(user);
		dto.setUserId(user.getId());
		dto.setUserName(user.getName());
		if (tokenFirebase != null && !user.getDevices().contains(tokenFirebase)) {
			user.getDevices().add(tokenFirebase);
		}
		
		return dto;
	}

	public LoginResultDTO loginEmailToken(String login, String emailToken) {
		UserAuth user = repository.findByLogin(login);
		if (!Objects.equals(user.getEmailToken(), emailToken)) {
			throw new UnauthorizedException();
		}
		if (user.isDeleted()) {
			throw new UserDisabledException();
		}

		LoginResultDTO dto = tokenService.createTokenForUser(user);
		dto.setUserId(user.getId());
		dto.setUserName(user.getName());
		return dto;
	}

	public Integer sendEmailToken(String login) {
		UserAuth user = repository.findByLogin(login);
		if (user == null) {
			user = new UserAuth();
			user.setLogin(login);
			user.setCreatedAt(new Date());

		}
		if (user.getRoles() == null || user.getRoles().isEmpty()) {
			List<RoleUser> roles = new ArrayList<>();
			roles.add(RoleUser.CUSTOMER);
			user.setRoles(roles);
		}
		Integer random = 100000 + new Random().nextInt(900000);
		user.setEmailToken(random.toString());
		user = repository.save(user);
		Tenant tenant = new Tenant();
		tenant.setId(user.getId());
		tenant.setName(user.getLogin());
		tenant.setCreatedAt(new Date());
		tenantRepository.save(tenant);
		user.setTenant(tenant);
		repository.save(user);
		String emailBody = "<div style='font-family: Arial, sans-serif;'><p>Olá!</p><p>Estou muito feliz de ter você nessa jornada de autoconhecimento e evolução com a Petália.</p><p>Logo abaixo está o token para você confirmar o seu email.</p><p style='font-size: 24px; font-weight: bold; margin: 20px 0;'>"+random.toString()+"</p><p>Muito obrigada!</p><p>Abraços<br>Equipe Petália</p>    </div>    ";
		emailService.sendEmail("no_reply@petalia.com.br", login, "Token de verificação", emailBody, true);
//		emailService.sendEmail("no_reply@petalia.com", login, "A Petália te dá as boas vindas!", emailBody, true);
		return random;
	}
	
	public LoginResultDTO refreshLogin(String refreshToken) {
		return tokenService.refreshToken(refreshToken);
	}

	public UserAuth findByLogin(String login) {
		if (repository.existsManyUsersByLogin(login)) {
			throw new UserDuplicatedException();
		}
		return repository.findByLogin(login);
	}
}
