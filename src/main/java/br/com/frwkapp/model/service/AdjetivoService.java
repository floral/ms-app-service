package br.com.frwkapp.model.service;

import java.util.List;
import java.util.stream.Collectors;

import br.com.frwkapp.model.domain.GrupoAdjetivo;
import br.com.frwkapp.model.domain.Tenant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Adjetivo;
import br.com.frwkapp.model.dto.AdjetivoDTO;
import br.com.frwkapp.model.repository.AdjetivoRepository;

@Service
public class AdjetivoService extends BaseService<Adjetivo, AdjetivoDTO> {

	@Autowired
	private AdjetivoRepository repository;
	@Autowired
	private GrupoAdjetivoService grupoAdjetivoService;

	@Override
	public BaseRepository<Adjetivo> getRepository() {
		return repository;
	}

	@Override
	public Adjetivo parseDtoToEntity(AdjetivoDTO dto) {
		Adjetivo entity = new Adjetivo();
		entity.setId(dto.getId());
		entity.setNome(dto.getNome());
		entity.setClassificacao(dto.getClassificacao());
		entity.setSignificado(dto.getSignificado());
		entity.setColocacaoPositiva(dto.getColocacaoPositiva());
		entity.setColocacaoNegativa(dto.getColocacaoNegativa());
		entity.setColocacaoNeutra(dto.getColocacaoNeutra());
		entity.setPergunta(dto.getPergunta());
		entity.setIconePositivo(dto.getIconePositivo());
		entity.setIconeNegativo(dto.getIconeNegativo());
		if (dto.getGrupo() != null) {
			entity.setGrupo(grupoAdjetivoService.parseDtoToEntity(dto.getGrupo()));
		}
		return entity;
	}
	
	@Override
    public List<AdjetivoDTO> parseToDTO(List<Adjetivo> list) {
        return list.stream().map(AdjetivoDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<AdjetivoDTO> parseToDTO(Page<Adjetivo> page) {
        return page.map(AdjetivoDTO::new);
    }

    @Override
    public AdjetivoDTO parseToDTO(Adjetivo entity) {
        return new AdjetivoDTO(entity);
    }

	@Override
	public List<Adjetivo> findAll(String search) {
		if (search == null) {
			return repository.findByOrderByNomeAsc();
		} else {
			return repository.findAllByIndexContainingIgnoreCaseOrderByNome(search);
		}
	}

	public List<Adjetivo> findAllByGrupo(Long grupoId) {
		return repository.findAllByGrupoOrderByNome(new GrupoAdjetivo(grupoId));
	}

	public List<Adjetivo> findAllByIds(List<Long> ids) {
		return repository.findAllByIdIn(ids);
	}
}
