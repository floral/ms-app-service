package br.com.frwkapp.model.service;

import br.com.frwkapp.abstracts.BaseMultiTenantService;
import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Cartao;
import br.com.frwkapp.model.domain.UserAuth;
import br.com.frwkapp.model.dto.CartaoDTO;
import br.com.frwkapp.model.repository.CartaoRepository;
import br.com.frwkapp.pagarme.PagarmeClient;
import br.com.frwkapp.pagarme.dto.CartaoPagarmeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CartaoService extends BaseMultiTenantService<Cartao, CartaoDTO> {

	@Autowired
	private CartaoRepository repository;

	@Override
	public BaseRepository<Cartao> getRepository() {
		return repository;
	}

	@Override
	public Cartao parseDtoToEntity(CartaoDTO dto) {
		Cartao cartao = new Cartao();
		cartao.setId(dto.getId());
		cartao.setIdPagarme(dto.getIdPagarme());
		cartao.setNome(dto.getNome());
		cartao.setUltimosQuatroNumeros(dto.getNumero().substring(dto.getNumero().length() - 4));
		cartao.setBandeira(dto.getBandeira());
		return cartao;
	}

	@Override
	public Cartao insert(CartaoDTO dto) {
		CartaoPagarmeDTO cartaoPagarmeDTO = new CartaoPagarmeDTO();
		cartaoPagarmeDTO.setNumber(dto.getNumero());
		cartaoPagarmeDTO.setExp_month(dto.getMes());
		cartaoPagarmeDTO.setExp_year(dto.getAno());
		cartaoPagarmeDTO.setHolder_name(dto.getTitular());
		cartaoPagarmeDTO.setHolder_document(dto.getCpf());
		cartaoPagarmeDTO.setCvv(dto.getCvv());
		cartaoPagarmeDTO.setBrand(dto.getBandeira());
		cartaoPagarmeDTO.setBilling_address_id(dto.getIdEnderecoPagarme());
		UserAuth userAuth = me();
		PagarmeClient pagarmeClient = new PagarmeClient();
		cartaoPagarmeDTO = pagarmeClient.cadastraCartaoCliente(userAuth.getIdClientePagarme(), cartaoPagarmeDTO);
		dto.setIdPagarme(cartaoPagarmeDTO.getId());
		return super.insert(dto);
	}

	@Override
    public List<CartaoDTO> parseToDTO(List<Cartao> list) {
        return list.stream().map(CartaoDTO::new).collect(Collectors.toList());
    }

    @Override
    public Page<CartaoDTO> parseToDTO(Page<Cartao> page) {
        return page.map(CartaoDTO::new);
    }

    @Override
    public CartaoDTO parseToDTO(Cartao entity) {
        return new CartaoDTO(entity);
    }
}
