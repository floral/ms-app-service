package br.com.frwkapp.model.repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.Endereco;
import br.com.frwkapp.model.domain.Pedido;
import br.com.frwkapp.model.domain.Tenant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PedidoRepository extends BaseRepository<Pedido> {

    Page<Pedido> findAllByTenantAndIndexContainingIgnoreCaseOrderByIdDesc(Tenant tenant, String indice, Pageable pageable);
    Page<Pedido> findAllByOrderByIdDesc(Pageable pageable);
}