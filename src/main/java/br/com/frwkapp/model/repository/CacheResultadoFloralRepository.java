package br.com.frwkapp.model.repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.CacheResultadoFloral;
import br.com.frwkapp.model.domain.Endereco;
import org.springframework.stereotype.Repository;


@Repository
public interface CacheResultadoFloralRepository extends BaseRepository<CacheResultadoFloral> {

    CacheResultadoFloral findOneByHash(String hash);
}