package br.com.frwkapp.model.repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.CupomDesconto;
import org.springframework.stereotype.Repository;


@Repository
public interface CupomDescontoRepository extends BaseRepository<CupomDesconto> {
    CupomDesconto findFirstByNomeAndAtivoIsTrue(String nome);
}