package br.com.frwkapp.model.repository;

import java.util.List;

import br.com.frwkapp.model.domain.Tenant;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.Floral;

@Repository
public interface FloralRepository extends BaseRepository<Floral> {

	List<Floral> findByOrderByNomeAsc();
	List<Floral> findAllByIndexContainingIgnoreCaseOrderByNome(String index);
	Floral findFirstByNomeContainingIgnoreCase(String nome);

	@Query(value = "select string_agg(cast(top.id as character varying), ',') from\n"
			+ "    (select sub.id, sum (sub.qt_sup + sub.qt_alc) qt, sum(sub.qt_sup) qt_sup, sum(sub.qt_alc) qt_alc  from "
			+ "        (select f.id, count(f.nome) qt_sup, 0 qt_alc from t_floral f, t_adjetivo a, t_floral_adjetivo fa "
			+ "        where f.id = fa.fk_floral "
			+ "        and a.id = fa.fk_adjetivo "
			+ "        and (fa.fk_adjetivo in (:superar) and objetivo = 'SUPERAR') "
			+ "        group by f.id "
			+ "        union all "
			+ "        select f.id, 0 qt_sup, count(f.nome) qt_alc from t_floral f, t_adjetivo a, t_floral_adjetivo fa "
			+ "        where f.id = fa.fk_floral "
			+ "        and a.id = fa.fk_adjetivo "
			+ "        and (fa.fk_adjetivo in (:alcancar) and objetivo = 'ALCANCAR') "
			+ "        group by f.id) sub "
			+ "    group by sub.id "
			+ "    order by qt desc, qt_sup desc, qt_alc desc "
			+ "    limit 4) top", nativeQuery = true)
	public String buscaFloralMaisIndicada(@Param("superar") List<Long> superar, @Param("alcancar") List<Long> alcancar);

	@Query(value = "select string_agg(cast(top.id as character varying), ',') from\n"
			+ "    (select sub.id, sum (sub.qt_sup + sub.qt_alc) qt, sum(sub.qt_sup) qt_sup, sum(sub.qt_alc) qt_alc  from "
			+ "        (select f.id, 0 qt_sup, count(f.nome) qt_alc from t_floral f, t_adjetivo a, t_floral_adjetivo fa "
			+ "        where f.id = fa.fk_floral "
			+ "        and a.id = fa.fk_adjetivo "
			+ "        and (fa.fk_adjetivo in (:alcancar) and objetivo = 'ALCANCAR') "
			+ "        group by f.id) sub "
			+ "    group by sub.id "
			+ "    order by qt desc, qt_sup desc, qt_alc desc "
			+ "    limit 4) top", nativeQuery = true)
	public String buscaFloralMaisIndicadaAlcancar(@Param("alcancar") List<Long> alcancar);

	@Query(value = "select string_agg(cast(top.id as character varying), ',') from\n"
			+ "    (select sub.id, sum (sub.qt_sup + sub.qt_alc) qt, sum(sub.qt_sup) qt_sup, sum(sub.qt_alc) qt_alc  from "
			+ "        (select f.id, 0 qt_sup, count(f.nome) qt_alc from t_floral f, t_adjetivo a, t_floral_adjetivo fa "
			+ "        where f.id = fa.fk_floral "
			+ "        and a.id = fa.fk_adjetivo "
			+ "        and (fa.fk_adjetivo in (:superar) and objetivo = 'SUPERAR') "
			+ "        group by f.id) sub "
			+ "    group by sub.id "
			+ "    order by qt desc, qt_sup desc, qt_alc desc "
			+ "    limit 4) top", nativeQuery = true)
	public String buscaFloralMaisIndicadaSuperar(@Param("superar") List<Long> superar);

	@Query(value = "select string_agg(cast(top.id as character varying), ',') from (select id from t_floral group by id order by embedding <-> CAST(:embedding AS vector) limit 3 ) top", nativeQuery = true)
	public String buscaFloralMaisIndicadaAI(@Param("embedding") String embedding);


}