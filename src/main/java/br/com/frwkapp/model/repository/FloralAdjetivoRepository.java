package br.com.frwkapp.model.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.Floral;
import br.com.frwkapp.model.domain.FloralAdjetivo;
import br.com.frwkapp.model.domain.enums.ObjetivoEnum;

@Repository
public interface FloralAdjetivoRepository extends BaseRepository<FloralAdjetivo> {

	public List<FloralAdjetivo> findAllByFloral(Floral floral);
	public List<FloralAdjetivo> findAllByFloralAndObjetivo(Floral floral, ObjetivoEnum objetivo);
}