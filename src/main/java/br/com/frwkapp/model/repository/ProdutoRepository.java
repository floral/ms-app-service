package br.com.frwkapp.model.repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.Produto;
import org.springframework.stereotype.Repository;


@Repository
public interface ProdutoRepository extends BaseRepository<Produto> {
}