package br.com.frwkapp.model.repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.Endereco;
import org.springframework.stereotype.Repository;


@Repository
public interface EnderecoRepository extends BaseRepository<Endereco> {
}