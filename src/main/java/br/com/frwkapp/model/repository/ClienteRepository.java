package br.com.frwkapp.model.repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.Cliente;
import br.com.frwkapp.model.domain.UserAuth;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;


@Repository
public interface ClienteRepository extends BaseRepository<Cliente> {

    Cliente findOneByUserAuth(UserAuth userAuth);

    Page<Cliente> findAllByOrderByIdDesc(Pageable pageable);
}