package br.com.frwkapp.model.repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.Pregacao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface PregacaoRepository extends BaseRepository<Pregacao> {

    @Query(value = "select p.transcricao from t_pregacao p where (vetores <-> CAST(:embedding AS vector)) < 1 order by vetores <-> CAST(:embedding AS vector) limit 1", nativeQuery = true)
    public String buscaPregacao(@Param("embedding") String embedding);
}