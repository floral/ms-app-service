package br.com.frwkapp.model.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.UserAuth;

@Repository
public interface UserRepository extends BaseRepository<UserAuth> {

	public UserAuth findByLoginAndPassword(String login, String password);
	public UserAuth findByLogin(String login);
	
	@Query(value = "select count(*) > 1 from t_user where login = :login and deleted <> true", nativeQuery = true)
	public boolean existsManyUsersByLogin(@Param("login") String login);
}