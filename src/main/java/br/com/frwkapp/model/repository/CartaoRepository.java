package br.com.frwkapp.model.repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.Cartao;
import org.springframework.stereotype.Repository;


@Repository
public interface CartaoRepository extends BaseRepository<Cartao> {
}