package br.com.frwkapp.model.repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.GrupoAdjetivo;
import br.com.frwkapp.model.domain.Tenant;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GrupoAdjetivoRepository extends BaseRepository<GrupoAdjetivo> {

    List<GrupoAdjetivo> findByOrderByNomeAsc();
    List<GrupoAdjetivo> findAllByIndexContainingIgnoreCaseOrderByNome(String index);
}