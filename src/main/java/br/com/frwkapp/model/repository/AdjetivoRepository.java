package br.com.frwkapp.model.repository;

import br.com.frwkapp.model.domain.GrupoAdjetivo;
import br.com.frwkapp.model.domain.Tenant;
import org.springframework.stereotype.Repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.Adjetivo;

import java.util.List;

@Repository
public interface AdjetivoRepository extends BaseRepository<Adjetivo> {

    List<Adjetivo> findByOrderByNomeAsc();
    List<Adjetivo> findAllByIndexContainingIgnoreCaseOrderByNome(String index);
    List<Adjetivo> findAllByGrupoOrderByNome(GrupoAdjetivo grupo);
    List<Adjetivo> findAllByIdIn(List<Long> ids);
}