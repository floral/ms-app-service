package br.com.frwkapp.model.repository;

import org.springframework.stereotype.Repository;

import br.com.frwkapp.abstracts.BaseRepository;
import br.com.frwkapp.model.domain.Tenant;

@Repository
public interface TenantRepository extends BaseRepository<Tenant> {

	public Tenant findByDomain(String domain);
}