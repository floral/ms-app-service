package br.com.frwkapp.model.domain;


import br.com.frwkapp.abstracts.BaseEntity;
import br.com.frwkapp.model.domain.enums.ClassificacaoAdjetivoEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_endereco")
public class Endereco extends BaseEntity {


    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(generator = "sq_endereco")
    @SequenceGenerator(name = "sq_endereco", sequenceName = "sq_endereco", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "id_pagarme")
    private String idPagarme;

    @Column(name = "logradouro")
    private String logradouro;

    @Column(name = "numero")
    private String numero;

    @Column(name = "bairro")
    private String bairro;

    @Column(name = "cep")
    private String cep;

    @Column(name = "complemento")
    private String complemento;

    @Column(name = "cidade")
    private String cidade;

    @Column(name = "uf")
    private String uf;

    public Endereco(Long id) {
    	this.id = id;
    }

}