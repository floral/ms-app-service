package br.com.frwkapp.model.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.frwkapp.abstracts.BaseEntity;
import br.com.frwkapp.abstracts.BuildIndex;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_floral")
public class Floral extends BaseEntity {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(generator = "sq_floral")
    @SequenceGenerator(name = "sq_floral", sequenceName = "sq_floral", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @BuildIndex
    @Column(name = "nome")
    private String nome;

    @BuildIndex
    @Column(name = "indicacao")
    private String indicacao;

    @Column(name = "texto_completo")
    private String textoCompleto;

    @BuildIndex
    @Column(name = "exemplo")
    private String exemplo;

    @BuildIndex
    @Column(name = "conclusao")
    private String conclusao;

    @BuildIndex
    @Column(name = "texto_gpt")
    private String textoGPT;

    public Floral(Long id) {
    	this.id = id;
    }

}