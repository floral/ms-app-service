package br.com.frwkapp.model.domain;

import br.com.frwkapp.abstracts.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_pregacao")
public class Pregacao extends BaseEntity {
    @Id
    @GeneratedValue(generator = "sq_pregacao")
    @SequenceGenerator(name = "sq_pregacao", sequenceName = "sq_pregacao", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "pregacao")
    private String pregacao;

    @Column(name = "titulo")
    private String titulo;

    @Column(name = "referencias_biblicas")
    private String referenciasBiblicas;

    @Column(name = "introducao")
    private String introducao;

    @Column(name = "mensagem_central")
    private String mensagemCentral;

    @Column(name = "aplicacao")
    private String aplicacao;

    @Column(name = "conclusao")
    private String conclusao;

    @Column(name = "oracao")
    private String oracao;

    @Column(name = "resumo")
    private String resumo;

    @Column(name = "transcricao")
    private String transcricao;

}
