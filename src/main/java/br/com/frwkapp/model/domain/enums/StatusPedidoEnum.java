package br.com.frwkapp.model.domain.enums;

public enum StatusPedidoEnum {

	AGUARDANDO_PAGAMENTO, PAGAMENTO_CONFIRMADO, EM_PRODUCAO, ENVIADO, CONCLUIDO, CANCELADO;

	public String getLabel() {
		switch (this) {
		case AGUARDANDO_PAGAMENTO:
			return "Aguardando Pagamento";
		case PAGAMENTO_CONFIRMADO:
			return "Pagamento Confirmado";
			case EM_PRODUCAO:
				return "Em Produção";
			case ENVIADO:
				return "Enviado";
			case CONCLUIDO:
				return "Concluído";
			case CANCELADO:
				return "Cancelado";
		default:
			return null;
		}
	}
}
