package br.com.frwkapp.model.domain.enums;

public enum RoleUser {

	ADMIN, GUEST, CUSTOMER;


	public static RoleUser getEnum(String label) {
		for (RoleUser role : values()) {
            if(role.toString().equals(label))
            	return role;
		}
		return RoleUser.GUEST;
	}
}
