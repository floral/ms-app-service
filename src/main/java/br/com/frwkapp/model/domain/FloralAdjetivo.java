package br.com.frwkapp.model.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.frwkapp.abstracts.BaseEntity;
import br.com.frwkapp.model.domain.enums.ObjetivoEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_floral_adjetivo")
public class FloralAdjetivo extends BaseEntity {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(generator = "sq_floral_adjetivo")
    @SequenceGenerator(name = "sq_floral_adjetivo", sequenceName = "sq_floral_adjetivo", allocationSize = 1)
    @Column(name = "id")
    private Long id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "fk_floral", referencedColumnName = "id")
    private Floral floral;

	@ManyToOne(optional = false)
	@JoinColumn(name = "fk_adjetivo", referencedColumnName = "id")
    private Adjetivo adjetivo;

    @Enumerated(EnumType.STRING)
    @Column(name = "objetivo")
    private ObjetivoEnum objetivo;

}