package br.com.frwkapp.model.domain;


import br.com.frwkapp.abstracts.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_cartao")
public class Cartao extends BaseEntity {

	@Id
    @GeneratedValue(generator = "sq_cartao")
    @SequenceGenerator(name = "sq_cartao", sequenceName = "sq_cartao", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "id_pagarme")
    private String idPagarme;

    @Column(name = "nome")
    private String nome;

    @Column(name = "ultimos_quatro_numeros")
    private String ultimosQuatroNumeros;

    @Column(name = "bandeira")
    private String bandeira;

    public Cartao(Long id) {
    	this.id = id;
    }

}