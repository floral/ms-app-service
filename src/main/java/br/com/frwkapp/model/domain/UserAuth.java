package br.com.frwkapp.model.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.frwkapp.abstracts.BaseEntity;
import br.com.frwkapp.model.domain.enums.RoleUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_user")
public class UserAuth extends BaseEntity {

	@Id
    @GeneratedValue(generator = "sq_user")
    @SequenceGenerator(name = "sq_user", sequenceName = "sq_user", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "name")
    private String name;

    @Column(name = "token_validacao_email")
    private String emailToken;

    @Column(name = "id_cliente_pagarme")
    private String idClientePagarme;

    @ElementCollection
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "t_user_role", joinColumns = @JoinColumn(name="fk_user"), uniqueConstraints = 
    		@UniqueConstraint(name = "UK_user_role",
    	    columnNames = {"fk_user", "role"}))
    @Column(name="role")
    private List<RoleUser> roles;
    
    @ElementCollection
    @CollectionTable(name = "t_user_device", joinColumns = @JoinColumn(name="fk_user"))
    @Column(name="device")
    private List<String> devices = new ArrayList<>();
    
    public UserAuth() {
    	roles = new ArrayList<>();
    }

    public UserAuth(String username) {
    	this.login = username;
    }
}