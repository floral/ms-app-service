package br.com.frwkapp.model.domain;


import javax.persistence.*;

import br.com.frwkapp.abstracts.BaseEntity;
import br.com.frwkapp.model.domain.enums.ClassificacaoAdjetivoEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_adjetivo")
public class Adjetivo extends BaseEntity {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(generator = "sq_adjetivo")
    @SequenceGenerator(name = "sq_adjetivo", sequenceName = "sq_adjetivo", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Enumerated(EnumType.STRING)
    @Column(name = "classificacao")
    private ClassificacaoAdjetivoEnum classificacao;

    @Column(name = "significado")
    private String significado;

    @Column(name = "colocacao_positiva")
    private String colocacaoPositiva;

    @Column(name = "colocacao_negativa")
    private String colocacaoNegativa;

    @Column(name = "colocacao_neutra")
    private String colocacaoNeutra;

    @Column(name = "pergunta")
    private String pergunta;

    @Column(name = "iconePositivo")
    private String iconePositivo;

    @Column(name = "iconeNegativo")
    private String iconeNegativo;


    @ManyToOne
    @JoinColumn(name = "fk_grupo_adjetivo", referencedColumnName = "id")
    private GrupoAdjetivo grupo;

    public Adjetivo(Long id) {
    	this.id = id;
    }

}