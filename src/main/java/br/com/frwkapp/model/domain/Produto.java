package br.com.frwkapp.model.domain;

import br.com.frwkapp.abstracts.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_produto")
public class Produto extends BaseEntity {

    @Id
    @GeneratedValue(generator = "sq_produto")
    @SequenceGenerator(name = "sq_produto", sequenceName = "sq_produto", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "descricao2")
    private String descricao2;

    @Column(name = "preco")
    private Double preco;

    @Column(name = "quantidade")
    private Integer quantidade;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "t_produto_floral",
            joinColumns = @JoinColumn(name = "fk_produto"),
            inverseJoinColumns = @JoinColumn(name = "fk_floral")
    )
    private List<Floral> florais = new ArrayList<>();

    public Produto(Long id) {
        this.id = id;
    }
}
