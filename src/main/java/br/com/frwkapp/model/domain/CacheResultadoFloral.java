package br.com.frwkapp.model.domain;


import br.com.frwkapp.abstracts.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_cache_resultado_floral")
public class CacheResultadoFloral extends BaseEntity {

	@Id
    @GeneratedValue(generator = "sq_cache_resultado_floral")
    @SequenceGenerator(name = "sq_cache_resultado_floral", sequenceName = "sq_cache_resultado_floral", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "hash")
    private String hash;

    @Column(name = "texto", columnDefinition = "TEXT")
    private String texto;

    @Column(name = "texto2", columnDefinition = "TEXT")
    private String texto2;

    public CacheResultadoFloral(Long id) {
    	this.id = id;
    }

}