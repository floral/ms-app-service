package br.com.frwkapp.model.domain;


import br.com.frwkapp.abstracts.BaseEntity;
import br.com.frwkapp.model.domain.enums.StatusPedidoEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_cliente")
public class Cliente extends BaseEntity {

	@Id
    @GeneratedValue(generator = "sq_cliente")
    @SequenceGenerator(name = "sq_cliente", sequenceName = "sq_cliente", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "id_pagarme")
    private String idPagarme;

    @Column(name = "nome_completo")
    private String nomeCompleto;

    @Column(name = "cpf")
    private String cpf;

    @Column(name = "data_nascimento")
    private Date dataNascimento;

    @Column(name = "numero_celular")
    private String numeroCelular;

    @ManyToOne(optional = false)
    @JoinColumn(name = "fk_user_auth", referencedColumnName = "id")
    private UserAuth userAuth;

    public Cliente(Long id) {
    	this.id = id;
    }

}