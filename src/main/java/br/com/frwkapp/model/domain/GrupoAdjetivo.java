package br.com.frwkapp.model.domain;


import br.com.frwkapp.abstracts.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_grupo_adjetivo")
public class GrupoAdjetivo extends BaseEntity {


    @Id
    @GeneratedValue(generator = "sq_grupo_adjetivo")
    @SequenceGenerator(name = "sq_grupo_adjetivo", sequenceName = "sq_grupo_adjetivo", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "descricao")
    private String descricao;

    public GrupoAdjetivo(Long id) {
    	this.id = id;
    }

}