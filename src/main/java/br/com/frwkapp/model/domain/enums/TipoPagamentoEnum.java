package br.com.frwkapp.model.domain.enums;

public enum TipoPagamentoEnum {

	CREDITO, PIX;

}
