package br.com.frwkapp.model.domain;


import br.com.frwkapp.abstracts.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_cupom_desconto")
public class CupomDesconto extends BaseEntity {

	@Id
    @GeneratedValue(generator = "sq_cupom_desconto")
    @SequenceGenerator(name = "sq_cupom_desconto", sequenceName = "sq_cupom_desconto", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "percentual_desconto")
    private Integer percentualDesconto;

    @Column(name = "data_validade")
    private Date dataValidade;

    @Column(name = "ativo")
    private boolean ativo;

    public CupomDesconto(Long id) {
    	this.id = id;
    }

}