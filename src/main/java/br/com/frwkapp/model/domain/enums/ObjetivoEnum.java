package br.com.frwkapp.model.domain.enums;

public enum ObjetivoEnum {

	SUPERAR, ALCANCAR;

	public String getLabel() {
		switch (this) {
		case SUPERAR:
			return "Superar";
		case ALCANCAR:
			return "Alcançar";
		default:
			return null;
		}
	}

//	public static ObjetivoEnum getEnum(String label) {
//		for (ObjetivoEnum hiring : values()) {
//            if(hiring.getLabel().equals(label))
//            	return hiring;
//		}
//		return ObjetivoEnum.ALCANCAR;
//	}

}
