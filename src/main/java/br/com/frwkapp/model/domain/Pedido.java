package br.com.frwkapp.model.domain;


import br.com.frwkapp.abstracts.BaseEntity;
import br.com.frwkapp.model.domain.enums.ObjetivoEnum;
import br.com.frwkapp.model.domain.enums.StatusPedidoEnum;
import br.com.frwkapp.model.domain.enums.TipoPagamentoEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "t_pedido")
public class Pedido extends BaseEntity {


	@Id
    @GeneratedValue(generator = "sq_pedido")
    @SequenceGenerator(name = "sq_pedido", sequenceName = "sq_pedido", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "id_pagarme")
    private String idPagarme;

    @ManyToOne(optional = false)
    @JoinColumn(name = "fk_user_auth", referencedColumnName = "id")
    private UserAuth userAuth;

    @ManyToOne(optional = false)
    @JoinColumn(name = "fk_produto", referencedColumnName = "id")
    private Produto produto;

    @ManyToOne(optional = true)
    @JoinColumn(name = "fk_cliente", referencedColumnName = "id")
    private Cliente cliente;

    @ManyToOne(optional = true)
    @JoinColumn(name = "fk_endereco", referencedColumnName = "id")
    private Endereco endereco;

    @ManyToOne(optional = true)
    @JoinColumn(name = "fk_cartao", referencedColumnName = "id")
    private Cartao cartao;

    @ManyToOne(optional = true)
    @JoinColumn(name = "fk_cupom_desconto", referencedColumnName = "id")
    private CupomDesconto cupomDesconto;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusPedidoEnum status;

    @Column(name = "preco_frete_selecionado")
    private Double precoFreteSelecionado;

    @Column(name = "id_frete_selecionado")
    private Integer idFreteSelecionado;

    @Column(name = "nome_frete_selecionado")
    private String nomeFreteSelecionado;

    @Column(name = "valor_total")
    private Double valorTotal;

    @Column(name = "valor_desconto")
    private Double valorDesconto;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_pagamento")
    private TipoPagamentoEnum tipoPagamento;

    @Column(name = "qrcode_copia_cola")
    private String qrcodeCopiacola;

    @Column(name = "qrcode_url")
    private String qrcodeUrl;

    public Pedido(Long id) {
    	this.id = id;
    }

}