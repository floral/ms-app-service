package br.com.frwkapp.model.domain.enums;

public enum ClassificacaoAdjetivoEnum {

	POSITIVO, NEGATIVO, NEUTRO;

}
