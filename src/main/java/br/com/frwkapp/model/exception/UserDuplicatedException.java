package br.com.frwkapp.model.exception;

public class UserDuplicatedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserDuplicatedException() {
		super("O email informado está sendo utilizado para mais de um usuário. Favor entrar em contato com o Administrador do sistema.");
	}

}
