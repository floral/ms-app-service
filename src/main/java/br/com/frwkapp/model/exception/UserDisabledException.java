package br.com.frwkapp.model.exception;

public class UserDisabledException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserDisabledException() {
		super("Usuário Inativo.");
	}

}
