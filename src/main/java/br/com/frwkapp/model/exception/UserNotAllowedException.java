package br.com.frwkapp.model.exception;

public class UserNotAllowedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserNotAllowedException() {
		super("Este e-mail não faz parte de um dos domínios da Framework. Tente novamente");
	}

}
