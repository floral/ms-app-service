package br.com.frwkapp.model.exception;

import org.hibernate.service.spi.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class UnauthenticatedException extends ServiceException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnauthenticatedException() {
        super("Unauthenticated");
    }
}
