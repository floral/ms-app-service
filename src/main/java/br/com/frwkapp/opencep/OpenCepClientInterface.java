package br.com.frwkapp.opencep;

import br.com.frwkapp.opencep.dto.OpenCepDTO;
import retrofit2.Call;
import retrofit2.http.*;

public interface OpenCepClientInterface {

    @GET("v1/{cep}")
    Call<OpenCepDTO> buscaEnderecoPorCEP(@Path("cep") String cep);
}