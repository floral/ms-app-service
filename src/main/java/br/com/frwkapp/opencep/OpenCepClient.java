package br.com.frwkapp.opencep;

import br.com.frwkapp.configuration.LoggingInterceptor;
import br.com.frwkapp.opencep.dto.OpenCepDTO;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.time.Duration;

public class OpenCepClient {

    OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new LoggingInterceptor())
            .connectTimeout(Duration.ofMinutes(1l))
            .readTimeout(Duration.ofMinutes(2l))
            .build();
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://opencep.com/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    OpenCepClientInterface service = retrofit.create(OpenCepClientInterface.class);

    public OpenCepDTO buscaEnderecoPorCEP(String cep) {
        Call<OpenCepDTO> call = service.buscaEnderecoPorCEP(cep);
        try {
            Response<OpenCepDTO> response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            } else {
                System.out.println("Response Error: " + response.errorBody().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

