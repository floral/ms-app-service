package br.com.frwkapp.opencep.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class OpenCepDTO {
    private String cep;
    private String logradouro;
    private String bairro;
    @SerializedName("localidade")
    private String cidade;
    private String uf;
}
