package br.com.frwkapp.abstracts;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseDTO implements Serializable {

	protected Long id;

	public BaseDTO(BaseEntity entity) {
		this.id = entity.getId();
	}

}
