package br.com.frwkapp.abstracts;

import br.com.frwkapp.model.domain.Tenant;
import br.com.frwkapp.model.domain.UserAuth;
import br.com.frwkapp.model.service.UserService;
import br.com.frwkapp.pagarme.PagarmeClient;
import br.com.frwkapp.pagarme.dto.ClientePagarmeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Date;
import java.util.List;

public abstract class BaseMultiTenantService<E extends BaseEntity, D extends BaseDTO> {

	public abstract BaseRepository<E> getRepository();

	@Autowired
	private UserService userService;

	public UserAuth me() {
		UserAuth userAuth = userService.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		if (userAuth != null && userAuth.getIdClientePagarme() == null) {
			PagarmeClient pagarmeClient = new PagarmeClient();
			ClientePagarmeDTO dto = new ClientePagarmeDTO();
			dto.setEmail(userAuth.getLogin());
			if (userAuth.getName() != null) {
				dto.setName(userAuth.getName());
			} else {
				dto.setName(userAuth.getLogin().split("@")[0]);
			}
			dto = pagarmeClient.cadastraCliente(dto);
			userAuth.setIdClientePagarme(dto.getId());
			userService.update(userAuth);
		}
		return userAuth;
	}

	protected Tenant getTenant() {
		try {
			UserAuth user = me();
			return user.getTenant();
		} catch (Throwable t) {
		}
		return null;
	}

	public E findOne(Tenant tenant, Long id) {
		return getRepository().findFirstByTenantAndId(tenant, id);
	}

	public Page<E> findAll(Tenant tenant, String search, Pageable pageable) {
		if (search != null) {
			return getRepository().findAllByTenantAndIndexContainingIgnoreCaseOrderByIdDesc(tenant, search, pageable);
		} else {
			return getRepository().findAllByTenantOrderByIdDesc(tenant, pageable);
		}
	}

	public List<E> findAll(Tenant empresa) {
		return getRepository().findAllByTenantOrderByIdDesc(empresa);
	}

	public List<E> findAll(Tenant tenant, String search) {
		if (search != null) {
			return getRepository().findAllByTenantAndIndexContainingIgnoreCaseOrderByIdDesc(tenant, search);
		} else {
			return getRepository().findAllByTenantOrderByIdDesc(tenant);
		}
	}

	public Page<E> findAll(Tenant empresa, Pageable pageable) {
		return getRepository().findAllByTenantOrderByIdDesc(empresa, pageable);
	}

	public E insert(E entity) {
		if (validate(entity)) {
			entity.setIndex(entity.buildIndex());
			entity.setCreatedAt(new Date());
			if (entity.getTenant() == null) {
				entity.setTenant(getTenant());
			}
			getRepository().save(entity);
		}
		return entity;
	}

	public E update(E entity) {
		if (validate(entity)) {
			entity.setIndex(entity.buildIndex());
			entity.setLastUpdate(new Date());
			if (entity.getTenant() == null) {
				entity.setTenant(getTenant());
			}
			getRepository().save(entity);
		}
		return entity;
	}

	public E insert(D dto) {
		return insert(parseDtoToEntity(dto));
	}

	public E update(D dto) {
		return update(parseDtoToEntity(dto));
	}

	public void flush() {
		getRepository().flush();
	}

	public void delete(Tenant tenant, Long id) {
		getRepository().deleteByTenantAndId(tenant, id);
	}

	public boolean validate(E entity) {
		return true;
	}
	public abstract E parseDtoToEntity(D dto);
	public abstract D parseToDTO(E entity);
	public abstract List<D> parseToDTO(List<E> page);
	public abstract Page<D> parseToDTO(Page<E> page);

}