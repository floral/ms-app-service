package br.com.frwkapp.abstracts;

import com.sendgrid.*;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class EmailService {

    public void sendEmail(String from, String to, String subject, String body, boolean isHtml) {
        Email emailFrom = new Email(from);
        Email emailTo = new Email(to);
        Content content = new Content(isHtml ? "text/html" : "text/plain", body);
        Mail mail = new Mail(emailFrom, subject, emailTo, content);
        SendGrid sg = new SendGrid("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");
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            sg.api(request);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}