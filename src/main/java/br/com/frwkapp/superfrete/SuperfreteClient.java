package br.com.frwkapp.superfrete;

import br.com.frwkapp.configuration.LoggingInterceptor;
import br.com.frwkapp.superfrete.dto.CotacaoFreteRequestDTO;
import br.com.frwkapp.superfrete.dto.CotacaoFreteResponseDTO;
import br.com.frwkapp.superfrete.dto.PedidoFreteRequestDTO;
import br.com.frwkapp.superfrete.dto.PedidoFreteResponseDTO;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.time.Duration;
import java.util.List;

public class SuperfreteClient {

    OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new LoggingInterceptor())
            .connectTimeout(Duration.ofMinutes(1l))
            .readTimeout(Duration.ofMinutes(2l))
            .build();
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://sandbox.superfrete.com/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    SuperfreteClientInterface service = retrofit.create(SuperfreteClientInterface.class);

    public List<CotacaoFreteResponseDTO> calculaFrete(String cep) {
        CotacaoFreteRequestDTO body = new CotacaoFreteRequestDTO(cep);
        Call<List<CotacaoFreteResponseDTO>> call = service.calculaFrete(body);
        try {
            Response<List<CotacaoFreteResponseDTO>> response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            } else {
                System.out.println("Response Error: " + response.errorBody().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public PedidoFreteResponseDTO criaPedidoFrete(PedidoFreteRequestDTO pedido) {
        Call<PedidoFreteResponseDTO> call = service.criaPedidoFrete(pedido);
        try {
            Response<PedidoFreteResponseDTO> response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            } else {
                System.out.println("Response Error: " + response.errorBody().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}

