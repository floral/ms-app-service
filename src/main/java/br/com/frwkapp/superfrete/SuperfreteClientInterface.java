package br.com.frwkapp.superfrete;

import br.com.frwkapp.superfrete.dto.CotacaoFreteRequestDTO;
import br.com.frwkapp.superfrete.dto.CotacaoFreteResponseDTO;
import br.com.frwkapp.superfrete.dto.PedidoFreteRequestDTO;
import br.com.frwkapp.superfrete.dto.PedidoFreteResponseDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

import java.util.List;

public interface SuperfreteClientInterface {
    @Headers({
            "User-Agent: Petalia (geraldoneto.ads@gmail.com)",
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE3MTU2MzA4MjUsInN1YiI6Impxdll4R3JkMHJPQTVqaU1mTzlFTTdsMUVjSDIifQ.WlDa6qOT232VOtWnRHe-HdAAdvgcj0pExrgdUGlZHsI"
    })
    @POST("api/v0/calculator")
    Call<List<CotacaoFreteResponseDTO>> calculaFrete(@Body CotacaoFreteRequestDTO body);

    @Headers({
            "User-Agent: Petalia (geraldoneto.ads@gmail.com)",
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE3MTU2MzA4MjUsInN1YiI6Impxdll4R3JkMHJPQTVqaU1mTzlFTTdsMUVjSDIifQ.WlDa6qOT232VOtWnRHe-HdAAdvgcj0pExrgdUGlZHsI"
    })
    @POST("api/v0/cart")
    Call<PedidoFreteResponseDTO> criaPedidoFrete(@Body PedidoFreteRequestDTO body);
}