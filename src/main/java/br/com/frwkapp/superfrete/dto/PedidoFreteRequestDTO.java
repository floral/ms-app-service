package br.com.frwkapp.superfrete.dto;

import br.com.frwkapp.model.dto.PedidoDTO;

import java.util.ArrayList;
import java.util.List;

public class PedidoFreteRequestDTO {
    private Location from;
    private Location to;
    private int service;
    private List<Product> products;
    private Volume volumes;
    private Options options;
    private String platform;

    public PedidoFreteRequestDTO() {}

    public PedidoFreteRequestDTO(PedidoDTO pedido) {
        setFrom(new Location("Petalia", "Rua do Ouro", "Serra", "Belo Horizonte", "MG", "30220000","admin@petalia.com.br"));
        setTo(new Location(pedido.getEndereco().getNome(), pedido.getEndereco().getLogradouro(), pedido.getEndereco().getBairro(), pedido.getEndereco().getCidade(), pedido.getEndereco().getUf(), pedido.getEndereco().getCep(), pedido.getEndereco().getEmail()));
        setService(pedido.getIdFreteSelecionado());
        List<Product> produtos = new ArrayList<>();
        Product product = new Product();
        product.setName(pedido.getProduto().getDescricao());
        product.setQuantity(1);
        product.setUnitary_value(pedido.getProduto().getPreco());
        produtos.add(product);
        setProducts(produtos);
        setVolumes(new Volume(5, 5, 5, 0.1));
        setOptions(new Options("NF_AQUI"));
        setPlatform("Petalia");
    }

    // Getters e Setters
    public Location getFrom() { return from; }
    public void setFrom(Location from) { this.from = from; }
    public Location getTo() { return to; }
    public void setTo(Location to) { this.to = to; }
    public int getService() { return service; }
    public void setService(int service) { this.service = service; }
    public List<Product> getProducts() { return products; }
    public void setProducts(List<Product> products) { this.products = products; }
    public Volume getVolumes() { return volumes; }
    public void setVolumes(Volume volumes) { this.volumes = volumes; }
    public Options getOptions() { return options; }
    public void setOptions(Options options) { this.options = options; }
    public String getPlatform() { return platform; }
    public void setPlatform(String platform) { this.platform = platform; }

    public static class Location {
        private String name;
        private String address;
        private String district;
        private String city;
        private String state_abbr;
        private String postal_code;
        private String email;

        public Location() {}

        public Location(String name, String address, String district, String city, String state_abbr, String postal_code, String email) {
            this.name = name;
            this.address = address;
            this.district = district;
            this.city = city;
            this.state_abbr = state_abbr;
            this.postal_code = postal_code;
            this.email = email;
        }

        // Getters e Setters
        public String getName() { return name; }
        public void setName(String name) { this.name = name; }
        public String getAddress() { return address; }
        public void setAddress(String address) { this.address = address; }
        public String getDistrict() { return district; }
        public void setDistrict(String district) { this.district = district; }
        public String getCity() { return city; }
        public void setCity(String city) { this.city = city; }
        public String getState_abbr() { return state_abbr; }
        public void setState_abbr(String state_abbr) { this.state_abbr = state_abbr; }
        public String getPostal_code() { return postal_code; }
        public void setPostal_code(String postal_code) { this.postal_code = postal_code; }
        public String getEmail() { return email; }
        public void setEmail(String email) { this.email = email; }
    }

    public static class Product {
        private String name;
        private int quantity;
        private double unitary_value;

        // Getters e Setters
        public String getName() { return name; }
        public void setName(String name) { this.name = name; }
        public int getQuantity() { return quantity; }
        public void setQuantity(int quantity) { this.quantity = quantity; }
        public double getUnitary_value() { return unitary_value; }
        public void setUnitary_value(double unitary_value) { this.unitary_value = unitary_value; }
    }

    public static class Volume {
        private int height;
        private int width;
        private int length;
        private double weight;

        public Volume() {}

        public Volume(int height, int width, int length, double weight) {
            setHeight(height);
            setWeight(width);
            setLength(length);
            setWeight(weight);
        }

        // Getters e Setters
        public int getHeight() { return height; }
        public void setHeight(int height) { this.height = height; }
        public int getWidth() { return width; }
        public void setWidth(int width) { this.width = width; }
        public int getLength() { return length; }
        public void setLength(int length) { this.length = length; }
        public double getWeight() { return weight; }
        public void setWeight(double weight) { this.weight = weight; }
    }

    public static class Options {
        private Invoice invoice;

        public Options() {}

        public Options(String key) {
            setInvoice(new Invoice(key));
        }
        // Getters e Setters
        public Invoice getInvoice() { return invoice; }
        public void setInvoice(Invoice invoice) { this.invoice = invoice; }

        public static class Invoice {
            private String key;

            public Invoice() {}
            public Invoice(String key) { this.key = key; }

            public String getKey() { return key; }
            public void setKey(String key) { this.key = key; }
        }
    }
}

