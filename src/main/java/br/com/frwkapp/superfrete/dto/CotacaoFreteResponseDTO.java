package br.com.frwkapp.superfrete.dto;

import java.util.List;

public class CotacaoFreteResponseDTO {
    private int id;
    private String name;
    private double price;
    private String discount;
    private String currency;
    private int delivery_time;
    private DeliveryRange delivery_range;
    private List<Package> packages;
    private AdditionalServices additional_services;
    private Company company;
    private boolean has_error;

    // Getters e Setters
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public double getPrice() { return price; }
    public void setPrice(double price) { this.price = price; }
    public String getDiscount() { return discount; }
    public void setDiscount(String discount) { this.discount = discount; }
    public String getCurrency() { return currency; }
    public void setCurrency(String currency) { this.currency = currency; }
    public int getDelivery_time() { return delivery_time; }
    public void setDelivery_time(int delivery_time) { this.delivery_time = delivery_time; }
    public DeliveryRange getDelivery_range() { return delivery_range; }
    public void setDelivery_range(DeliveryRange delivery_range) { this.delivery_range = delivery_range; }
    public List<Package> getPackages() { return packages; }
    public void setPackages(List<Package> packages) { this.packages = packages; }
    public AdditionalServices getAdditional_services() { return additional_services; }
    public void setAdditional_services(AdditionalServices additional_services) { this.additional_services = additional_services; }
    public Company getCompany() { return company; }
    public void setCompany(Company company) { this.company = company; }
    public boolean isHas_error() { return has_error; }
    public void setHas_error(boolean has_error) { this.has_error = has_error; }

    public static class DeliveryRange {
        private int min;
        private int max;

        public int getMin() { return min; }
        public void setMin(int min) { this.min = min; }
        public int getMax() { return max; }
        public void setMax(int max) { this.max = max; }
    }

    public static class Package {
        private double price;
        private String discount;
        private String format;
        private Dimensions dimensions;
        private String weight;
        private double insurance_value;

        public double getPrice() { return price; }
        public void setPrice(double price) { this.price = price; }
        public String getDiscount() { return discount; }
        public void setDiscount(String discount) { this.discount = discount; }
        public String getFormat() { return format; }
        public void setFormat(String format) { this.format = format; }
        public Dimensions getDimensions() { return dimensions; }
        public void setDimensions(Dimensions dimensions) { this.dimensions = dimensions; }
        public String getWeight() { return weight; }
        public void setWeight(String weight) { this.weight = weight; }
        public double getInsurance_value() { return insurance_value; }
        public void setInsurance_value(double insurance_value) { this.insurance_value = insurance_value; }

        public static class Dimensions {
            private String height;
            private String width;
            private String length;

            public String getHeight() { return height; }
            public void setHeight(String height) { this.height = height; }
            public String getWidth() { return width; }
            public void setWidth(String width) { this.width = width; }
            public String getLength() { return length; }
            public void setLength(String length) { this.length = length; }
        }
    }

    public static class AdditionalServices {
        private boolean receipt;
        private boolean own_hand;

        public boolean isReceipt() { return receipt; }
        public void setReceipt(boolean receipt) { this.receipt = receipt; }
        public boolean isOwn_hand() { return own_hand; }
        public void setOwn_hand(boolean own_hand) { this.own_hand = own_hand; }
    }

    public static class Company {
        private int id;
        private String name;
        private String picture;

        public int getId() { return id; }
        public void setId(int id) { this.id = id; }
        public String getName() { return name; }
        public void setName(String name) { this.name = name; }
        public String getPicture() { return picture; }
        public void setPicture(String picture) { this.picture = picture; }
    }
}

