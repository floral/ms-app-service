package br.com.frwkapp.superfrete.dto;

import com.google.gson.annotations.SerializedName;



public class CotacaoFreteRequestDTO {

    private Location from;
    private Location to;
    private String services;
    private Options options;
    @SerializedName("package")
    private Package pack;

    public CotacaoFreteRequestDTO() {}

    public CotacaoFreteRequestDTO(String cepPara) {
        setFrom(new Location("30220000"));
        setTo(new Location(cepPara));
        setServices("1, 2, 17");
        setOptions(new Options(false, false, 0d, false));
        setPack(new Package(50, 50, 50, 0.3));
    }

    public Location getFrom() {
        return from;
    }

    public void setFrom(Location from) {
        this.from = from;
    }

    public Location getTo() {
        return to;
    }

    public void setTo(Location to) {
        this.to = to;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public Options getOptions() {
        return options;
    }

    public void setOptions(Options options) {
        this.options = options;
    }

    public Package getPack() {
        return pack;
    }

    public void setPack(Package pack) {
        this.pack = pack;
    }

    public static class Location {
        private String postal_code;
        public Location(String postal_code) {
            this.postal_code = postal_code;
        }
        public String getPostal_code() {
            return postal_code;
        }
        public void setPostal_code(String postal_code) {
            this.postal_code = postal_code;
        }
    }

    // Options class
    public static class Options {
        private boolean own_hand;
        private boolean receipt;
        private double insurance_value;
        private boolean use_insurance_value;

        public Options(boolean own_hand, boolean receipt, double insurance_value, boolean use_insurance_value) {
            this.own_hand = own_hand;
            this.receipt = receipt;
            this.insurance_value = insurance_value;
            this.use_insurance_value = use_insurance_value;
        }

        public boolean isOwn_hand() {
            return own_hand;
        }

        public void setOwn_hand(boolean own_hand) {
            this.own_hand = own_hand;
        }

        public boolean isReceipt() {
            return receipt;
        }

        public void setReceipt(boolean receipt) {
            this.receipt = receipt;
        }

        public double getInsurance_value() {
            return insurance_value;
        }

        public void setInsurance_value(double insurance_value) {
            this.insurance_value = insurance_value;
        }

        public boolean isUse_insurance_value() {
            return use_insurance_value;
        }

        public void setUse_insurance_value(boolean use_insurance_value) {
            this.use_insurance_value = use_insurance_value;
        }
    }

    public static class Package {
        private int height;
        private int width;
        private int length;
        private double weight;

        public Package(int height, int width, int length, double weight) {
            this.height = height;
            this.width = width;
            this.length = length;
            this.weight = weight;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getLength() {
            return length;
        }

        public void setLength(int length) {
            this.length = length;
        }

        public double getWeight() {
            return weight;
        }

        public void setWeight(double weight) {
            this.weight = weight;
        }
    }
}

