package br.com.frwkapp.superfrete.dto;

public class PedidoFreteResponseDTO {

    private String id;
    private double price;
    private String protocol;
    private String self_tracking;
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getSelf_tracking() {
        return self_tracking;
    }

    public void setSelf_tracking(String self_tracking) {
        this.self_tracking = self_tracking;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

