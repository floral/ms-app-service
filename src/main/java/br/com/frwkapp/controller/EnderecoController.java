package br.com.frwkapp.controller;

import br.com.frwkapp.abstracts.BaseMultiTenantRestController;
import br.com.frwkapp.abstracts.BaseMultiTenantService;
import br.com.frwkapp.abstracts.BaseRestController;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Endereco;
import br.com.frwkapp.model.dto.EnderecoDTO;
import br.com.frwkapp.model.service.EnderecoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("endereco")
public class EnderecoController extends BaseMultiTenantRestController<Endereco, EnderecoDTO> {

	@Autowired
    private EnderecoService service;
    
	@Override
    protected BaseMultiTenantService<Endereco, EnderecoDTO> getService() {
        return service;
    }
	
	public List<EnderecoDTO> findAll(String search) {
		return super.findAll(search);
	}

}