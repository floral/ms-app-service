package br.com.frwkapp.controller;

import br.com.frwkapp.abstracts.BaseMultiTenantRestController;
import br.com.frwkapp.abstracts.BaseMultiTenantService;
import br.com.frwkapp.model.domain.Cartao;
import br.com.frwkapp.model.dto.CartaoDTO;
import br.com.frwkapp.model.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("cartao")
public class CartaoController extends BaseMultiTenantRestController<Cartao, CartaoDTO> {

	@Autowired
    private CartaoService service;
    
	@Override
    protected BaseMultiTenantService<Cartao, CartaoDTO> getService() {
        return service;
    }
	
	public List<CartaoDTO> findAll(String search) {
		return super.findAll(search);
	}

}