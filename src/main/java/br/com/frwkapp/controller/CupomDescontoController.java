package br.com.frwkapp.controller;

import br.com.frwkapp.abstracts.BaseResponseDTO;
import br.com.frwkapp.abstracts.BaseRestController;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.CupomDesconto;
import br.com.frwkapp.model.dto.CupomDescontoDTO;
import br.com.frwkapp.model.service.CupomDescontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("cupom-desconto")
public class CupomDescontoController extends BaseRestController<CupomDesconto, CupomDescontoDTO> {

	@Autowired
    private CupomDescontoService service;
    
	@Override
    protected BaseService<CupomDesconto, CupomDescontoDTO> getService() {
        return service;
    }
	
	public List<CupomDescontoDTO> findAll(String search) {
		return super.findAll(search);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/consultar/{cupom}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public BaseResponseDTO buscaPorNome(@PathVariable String cupom) {
		return buildResponse(service.parseToDTO(service.buscaPorNome(cupom)));
	}
}