package br.com.frwkapp.controller;

import br.com.frwkapp.abstracts.BaseRestController;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.GrupoAdjetivo;
import br.com.frwkapp.model.dto.GrupoAdjetivoDTO;
import br.com.frwkapp.model.service.GrupoAdjetivoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("grupo-adjetivo")
public class GrupoAdjetivoController extends BaseRestController<GrupoAdjetivo, GrupoAdjetivoDTO> {

	@Autowired
    private GrupoAdjetivoService service;
    
	@Override
    protected BaseService<GrupoAdjetivo, GrupoAdjetivoDTO> getService() {
        return service;
    }
	
	public List<GrupoAdjetivoDTO> findAll(String search) {
		return super.findAll(search);
	}

}