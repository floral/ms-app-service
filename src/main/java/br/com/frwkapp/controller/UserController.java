package br.com.frwkapp.controller;

import java.util.List;

import br.com.frwkapp.abstracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.frwkapp.model.domain.UserAuth;
import br.com.frwkapp.model.domain.enums.RoleUser;
import br.com.frwkapp.model.dto.UserDTO;
import br.com.frwkapp.model.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("user")
public class UserController extends BaseMultiTenantRestController<UserAuth, UserDTO> {

	@Autowired
    private UserService service;
    
	@Override
    protected BaseMultiTenantService<UserAuth, UserDTO> getService() {
        return service;
    }
	
	public List<UserDTO> findAll(String search) {
		return super.findAll(search);
	}
	
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Find all roles user")
	@RequestMapping(value = "/roles", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    public BaseResponseDTO findAllRolesUser() {
		return buildResponse(RoleUser.values());
	}
}