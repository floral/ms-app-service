package br.com.frwkapp.controller;

import br.com.frwkapp.abstracts.BaseMultiTenantRestController;
import br.com.frwkapp.abstracts.BaseMultiTenantService;
import br.com.frwkapp.abstracts.BaseResponseDTO;
import br.com.frwkapp.model.domain.Cliente;
import br.com.frwkapp.model.domain.UserAuth;
import br.com.frwkapp.model.domain.enums.RoleUser;
import br.com.frwkapp.model.dto.ClienteDTO;
import br.com.frwkapp.model.service.ClienteService;
import br.com.frwkapp.pagarme.PagarmeClient;
import br.com.frwkapp.pagarme.dto.ClientePagarmeDTO;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;

@RestController
@RequestMapping("cliente")
public class ClienteController extends BaseMultiTenantRestController<Cliente, ClienteDTO> {

	@Autowired
    private ClienteService service;
    
	@Override
    protected BaseMultiTenantService<Cliente, ClienteDTO> getService() {
        return service;
    }

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/me", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public BaseResponseDTO buscarCliente() {
		UserAuth userAuth = service.me();
		Cliente cliente = service.buscarPorUserAuth(userAuth);
		if (cliente != null) {
			return buildResponse(service.parseToDTO(cliente));
		} else {
			return buildResponse(null);
		}
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/completar/cadastro", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public BaseResponseDTO completarCadastro(@RequestBody ClienteDTO dto) {
		Cliente cliente = service.findOne(getTenant(), dto.getId());
		if (cliente == null) {
			cliente = service.parseDtoToEntity(dto);
			cliente.setUserAuth(service.me());
			cliente = service.insert(cliente);
		} else {
			cliente.setNomeCompleto(dto.getNomeCompleto());
			cliente.setCpf(dto.getCpf());
			cliente.setNumeroCelular(dto.getNumeroCelular());
			cliente.setDataNascimento(dto.getDataNascimento());
			cliente = service.update(cliente);
		}

		ClientePagarmeDTO clientePagarmeDTO = new ClientePagarmeDTO();
		UserAuth userAuth = me();
		clientePagarmeDTO.setId(me().getIdClientePagarme());
		clientePagarmeDTO.setEmail(userAuth.getLogin());
		clientePagarmeDTO.setDocument_type("CPF");
		clientePagarmeDTO.setType("individual");
		clientePagarmeDTO.setDocument(cliente.getCpf());
		clientePagarmeDTO.setName(cliente.getNomeCompleto());
		ClientePagarmeDTO.Phones phones = new ClientePagarmeDTO.Phones();
		ClientePagarmeDTO.Phones.Phone phone = new ClientePagarmeDTO.Phones.Phone();
		phone.setCountry_code("55");
		phone.setArea_code(cliente.getNumeroCelular().substring(0, 2));
		phone.setNumber(cliente.getNumeroCelular().substring(2));
		phones.setMobile_phone(phone);
		clientePagarmeDTO.setPhones(phones);
		clientePagarmeDTO.setBirthdate(new SimpleDateFormat("MM/dd/yyyy").format(cliente.getDataNascimento()));
		PagarmeClient pagarmeClient = new PagarmeClient();
		pagarmeClient.atualizaCliente(clientePagarmeDTO);

		return buildResponse(service.parseToDTO(cliente));
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/list-clientes-admin", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public BaseResponseDTO listClientesAdmin(Pageable pageable) throws Exception {
		if (!me().getRoles().contains(RoleUser.ADMIN)) {
			throw new RuntimeException("Not authorized");
		}
		return buildResponse(service.listClientesAdmin(pageable));
	}
}