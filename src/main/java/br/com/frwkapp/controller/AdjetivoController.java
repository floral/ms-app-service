package br.com.frwkapp.controller;

import java.util.List;

import br.com.frwkapp.model.dto.PregacaoDTO;
import br.com.frwkapp.model.dto.RelacionamentosDTO;
import br.com.frwkapp.model.service.PregacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import br.com.frwkapp.abstracts.BaseRestController;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Adjetivo;
import br.com.frwkapp.model.dto.AdjetivoDTO;
import br.com.frwkapp.model.service.AdjetivoService;

@RestController
@RequestMapping("adjetivo")
public class AdjetivoController extends BaseRestController<Adjetivo, AdjetivoDTO> {

	@Autowired
    private AdjetivoService service;
	@Autowired
	private PregacaoService pregacaoService;
    
	@Override
    protected BaseService<Adjetivo, AdjetivoDTO> getService() {
        return service;
    }
	
	public List<AdjetivoDTO> findAll(String search) {
		return super.findAll(search);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/busca-adjetivos-agrupamento", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public List<AdjetivoDTO> buscaAdjetivosAgrupamento(@RequestParam(name = "grupoId") Long id) {
		List<Adjetivo> list = service.findAllByGrupo(id);
		return service.parseToDTO(list);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/processa/pregacoes/gpt", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public String processaPregacoes() {
		return pregacaoService.buildEmbeddings();
	}
}