package br.com.frwkapp.controller;

import br.com.frwkapp.abstracts.BaseRestController;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Pregacao;
import br.com.frwkapp.model.dto.PregacaoDTO;
import br.com.frwkapp.model.service.PregacaoService;
import br.com.frwkapp.model.service.PregacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("pastor")
public class PastorController extends BaseRestController<Pregacao, PregacaoDTO> {

	@Autowired
    private PregacaoService service;
	
	@Override
    protected BaseService<Pregacao, PregacaoDTO> getService() {
        return service;
    }
	
	public List<PregacaoDTO> findAll(String search) {
		return super.findAll(search);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/prompt-reflection", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public String prompt(@RequestParam(name = "prompt") String prompt, @RequestParam(name = "idUsuario") String idUsuario) {
		return service.prompt(prompt, idUsuario);
	}

}