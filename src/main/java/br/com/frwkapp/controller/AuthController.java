package br.com.frwkapp.controller;

import br.com.frwkapp.model.domain.UserAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import br.com.frwkapp.abstracts.BaseResponseDTO;
import br.com.frwkapp.model.dto.LoginDTO;
import br.com.frwkapp.model.service.UserService;
import io.swagger.annotations.ApiImplicitParam;

@RestController
@RequestMapping("auth")
public class AuthController {

	@Autowired
    private UserService service;
	           
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public BaseResponseDTO login(@RequestBody LoginDTO dto) throws Exception {
    	return buildResponse(service.login(dto.getLogin(), dto.getPassword(), dto.getTokenFirebase()));
    }

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value="/send-email-token/{email}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public BaseResponseDTO sendEmailToken(@PathVariable String email) {
		return buildResponse(service.sendEmailToken(email));
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value="/login-email-token/{email}/{token}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public BaseResponseDTO loginEmailToken(@PathVariable String email, @PathVariable String token) {
		return buildResponse(service.loginEmailToken(email, token));
	}
    
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value="/refresh", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public BaseResponseDTO refresh(@RequestParam String refreshToken) {
    	return buildResponse(service.refreshLogin(refreshToken));
    }

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/me", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	public BaseResponseDTO me() {
		UserAuth userAuth = service.me();
		return buildResponse(service.parseToDTOAuth(userAuth));
	}

    protected BaseResponseDTO buildResponse(Object object) {
		BaseResponseDTO response = new BaseResponseDTO();
		response.setData(object);
		return response;
	}
}