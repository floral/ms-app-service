package br.com.frwkapp.controller;

import br.com.frwkapp.abstracts.*;
import br.com.frwkapp.model.domain.Pedido;
import br.com.frwkapp.model.domain.enums.RoleUser;
import br.com.frwkapp.model.dto.PedidoDTO;
import br.com.frwkapp.model.service.PedidoService;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.QueryParam;
import java.util.List;

@RestController
@RequestMapping("pedido")
public class PedidoController extends BaseMultiTenantRestController<Pedido, PedidoDTO> {

	@Autowired
    private PedidoService service;
    
	@Override
    protected BaseMultiTenantService<Pedido, PedidoDTO> getService() {
        return service;
    }

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
	@RequestMapping(value = "/checkout", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public BaseResponseDTO checkout(@RequestBody PedidoDTO dto) throws Exception {
		return buildResponse(service.checkout(dto));
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/opcoes-frete", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public BaseResponseDTO opcoesFrete(@RequestParam(value = "cep") String cep) throws Exception {
		return buildResponse(service.calculaFrete(cep));
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/busca-endereco-e-opcoes-frete", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public BaseResponseDTO buscaEnderecoOpcoesFrete(@RequestParam(value = "cep") String cep) throws Exception {
		return buildResponse(service.calculaFreteBuscaEndereco(cep));
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/list-pedidos-admin", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public BaseResponseDTO listPedidosAdmin(Pageable pageable) throws Exception {
		if (!me().getRoles().contains(RoleUser.ADMIN)) {
			throw new RuntimeException("Not authorized");
		}
		return buildResponse(service.listPedidosAdmin(pageable));
	}
}