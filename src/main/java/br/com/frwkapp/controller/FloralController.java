package br.com.frwkapp.controller;

import java.util.ArrayList;
import java.util.List;

import br.com.frwkapp.model.dto.RelacionamentosDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.frwkapp.abstracts.BaseResponseDTO;
import br.com.frwkapp.abstracts.BaseRestController;
import br.com.frwkapp.abstracts.BaseService;
import br.com.frwkapp.model.domain.Floral;
import br.com.frwkapp.model.dto.BuscaFloralDTO;
import br.com.frwkapp.model.dto.FloralAdjetivoDTO;
import br.com.frwkapp.model.dto.FloralDTO;
import br.com.frwkapp.model.service.FloralService;

@RestController
@RequestMapping("floral")
public class FloralController extends BaseRestController<Floral, FloralDTO> {

	@Autowired
    private FloralService service;
    
	@Override
    protected BaseService<Floral, FloralDTO> getService() {
        return service;
    }
	
	public List<FloralDTO> findAll(String search) {
		return super.findAll(search);
	}

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/gerar/ai/{busca}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public BaseResponseDTO gerarAI(@PathVariable String busca) throws Exception {
        return buildResponse(service.formulaProdutoAI(busca));
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/gerar/questionario", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public BaseResponseDTO gerarQuestionario(@RequestBody BuscaFloralDTO dto) throws Exception {
        return buildResponse(service.formulaProdutoQuestionario(dto.getSuperar(), dto.getAlcancar()));
    }
	
	@ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/get/completo", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public BaseResponseDTO getCompleto() throws Exception {
		List<Floral> result = service.findAll();
		List<FloralDTO> dtos = service.parseToDTO(result);
		dtos.forEach(dto -> {
			dto.setRelacionamentos(service.buscaRelacionamentos(dto.getId()));
		});
		
    	return buildResponse(dtos);
    }
	
	@ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}/busca-relacionamentos", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public RelacionamentosDTO buscaRelacionamento(@PathVariable Long id) throws Exception {
    	return service.buscaRelacionamentos(id);
    }
	
	@ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/salva-relacionamentos", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public BaseResponseDTO salvaRelacionamento(@RequestBody FloralAdjetivoDTO dto) throws Exception {
		if (dto.getAdjetivoId() == null && dto.getAdjetivoDTO() != null) {
			dto.setAdjetivoId(dto.getAdjetivoDTO().getId());
		}
    	return buildResponse(service.salvaRelacionamento(dto.getFloralId(), dto.getAdjetivoId(), dto.getObjetivo()));
    }
	
	@ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/atualiza-relacionamentos/{id}", method = RequestMethod.PUT, produces = "application/json;charset=UTF-8")
    public BaseResponseDTO atualizaRelacionamento(@PathVariable Long id, @RequestBody FloralAdjetivoDTO dto) throws Exception {
		if (dto.getAdjetivoId() == null && dto.getAdjetivoDTO() != null) {
			dto.setAdjetivoId(dto.getAdjetivoDTO().getId());
		}
    	return buildResponse(service.atualizaRelacionamento(id, dto.getFloralId(), dto.getAdjetivoId(), dto.getObjetivo()));
    }
	
	@ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/remove-relacionamentos/{id}", method = RequestMethod.DELETE, produces = "application/json;charset=UTF-8")
    public BaseResponseDTO removeRelacionamento(@PathVariable Long id) throws Exception {
    	return buildResponse(service.removeRelacionamento(id));
    }

}