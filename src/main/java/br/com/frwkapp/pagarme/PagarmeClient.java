package br.com.frwkapp.pagarme;

import br.com.frwkapp.configuration.LoggingInterceptor;
import br.com.frwkapp.pagarme.dto.CartaoPagarmeDTO;
import br.com.frwkapp.pagarme.dto.ClientePagarmeDTO;
import br.com.frwkapp.pagarme.dto.EnderecoPagarmeDTO;
import br.com.frwkapp.pagarme.dto.PedidoPagarmeDTO;
import br.com.frwkapp.superfrete.SuperfreteClientInterface;
import br.com.frwkapp.superfrete.dto.CotacaoFreteRequestDTO;
import br.com.frwkapp.superfrete.dto.CotacaoFreteResponseDTO;
import br.com.frwkapp.superfrete.dto.PedidoFreteRequestDTO;
import br.com.frwkapp.superfrete.dto.PedidoFreteResponseDTO;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;

import java.io.IOException;
import java.time.Duration;
import java.util.List;

public class PagarmeClient {

    OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new LoggingInterceptor())
            .connectTimeout(Duration.ofMinutes(1l))
            .readTimeout(Duration.ofMinutes(2l))
            .build();
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.pagar.me/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    PagarmeClientInterface service = retrofit.create(PagarmeClientInterface.class);

    public ClientePagarmeDTO cadastraCliente(ClientePagarmeDTO dto) {
        Call<ClientePagarmeDTO> call = service.cadastraCliente(dto);
        try {
            Response<ClientePagarmeDTO> response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            } else {
                System.out.println("Response Error: " + response.errorBody().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ClientePagarmeDTO atualizaCliente(ClientePagarmeDTO dto) {
        Call<ClientePagarmeDTO> call = service.atualizaCliente(dto.getId(), dto);
        try {
            Response<ClientePagarmeDTO> response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            } else {
                System.out.println("Response Error: " + response.errorBody().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public CartaoPagarmeDTO cadastraCartaoCliente(String idClientePagarme, CartaoPagarmeDTO dto) {
        Call<CartaoPagarmeDTO> call = service.cadastraCartaoCliente(idClientePagarme, dto);
        try {
            Response<CartaoPagarmeDTO> response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            } else {
                System.out.println("Response Error: " + response.errorBody().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public EnderecoPagarmeDTO cadastraEnderecoCliente(String idClientePagarme, EnderecoPagarmeDTO dto) {
        Call<EnderecoPagarmeDTO> call = service.cadastraEnderecoCliente(idClientePagarme, dto);
        try {
            Response<EnderecoPagarmeDTO> response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            } else {
                System.out.println("Response Error: " + response.errorBody().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public PedidoPagarmeDTO criaPedido(@Body PedidoPagarmeDTO body) {
        Call<PedidoPagarmeDTO> call = service.criaPedido(body);
        try {
            Response<PedidoPagarmeDTO> response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            } else {
                System.out.println("Response Error: " + response.errorBody().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

