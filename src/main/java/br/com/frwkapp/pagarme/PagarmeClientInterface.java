package br.com.frwkapp.pagarme;

import br.com.frwkapp.pagarme.dto.CartaoPagarmeDTO;
import br.com.frwkapp.pagarme.dto.ClientePagarmeDTO;
import br.com.frwkapp.pagarme.dto.EnderecoPagarmeDTO;
import br.com.frwkapp.pagarme.dto.PedidoPagarmeDTO;
import br.com.frwkapp.superfrete.dto.CotacaoFreteRequestDTO;
import br.com.frwkapp.superfrete.dto.CotacaoFreteResponseDTO;
import br.com.frwkapp.superfrete.dto.PedidoFreteRequestDTO;
import br.com.frwkapp.superfrete.dto.PedidoFreteResponseDTO;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;
/*
sk_9cfb93cf92314edeaca464be0b12a731
sk_test_a703048228284626bc55e87fd21cceed
 */
public interface PagarmeClientInterface {
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Basic c2tfdGVzdF9hNzAzMDQ4MjI4Mjg0NjI2YmM1NWU4N2ZkMjFjY2VlZDo="
    })
    @POST("core/v5/customers")
    Call<ClientePagarmeDTO> cadastraCliente(@Body ClientePagarmeDTO body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Basic c2tfdGVzdF9hNzAzMDQ4MjI4Mjg0NjI2YmM1NWU4N2ZkMjFjY2VlZDo="
    })
    @GET("core/v5/customers/{customer_id}")
    Call<ClientePagarmeDTO> buscaCliente(@Path("customer_id") String id);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Basic c2tfdGVzdF9hNzAzMDQ4MjI4Mjg0NjI2YmM1NWU4N2ZkMjFjY2VlZDo="
    })
    @PUT("core/v5/customers/{customer_id}")
    Call<ClientePagarmeDTO> atualizaCliente(@Path("customer_id") String id, @Body ClientePagarmeDTO body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Basic c2tfdGVzdF9hNzAzMDQ4MjI4Mjg0NjI2YmM1NWU4N2ZkMjFjY2VlZDo="
    })
    @POST("core/v5/customers/{customer_id}/cards")
    Call<CartaoPagarmeDTO> cadastraCartaoCliente(@Path("customer_id") String id, @Body CartaoPagarmeDTO body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Basic c2tfdGVzdF9hNzAzMDQ4MjI4Mjg0NjI2YmM1NWU4N2ZkMjFjY2VlZDo="
    })
    @POST("core/v5/customers/{customer_id}/addresses")
    Call<EnderecoPagarmeDTO> cadastraEnderecoCliente(@Path("customer_id") String id, @Body EnderecoPagarmeDTO body);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Basic c2tfdGVzdF9hNzAzMDQ4MjI4Mjg0NjI2YmM1NWU4N2ZkMjFjY2VlZDo="
    })
    @POST("core/v5/orders")
    Call<PedidoPagarmeDTO> criaPedido(@Body PedidoPagarmeDTO body);

}