package br.com.frwkapp.pagarme.dto;

import br.com.frwkapp.model.domain.Pedido;
import br.com.frwkapp.model.domain.enums.TipoPagamentoEnum;
import br.com.frwkapp.model.dto.PedidoDTO;

import java.util.ArrayList;
import java.util.List;

public class PedidoPagarmeDTO {

    private String id;
    private String customer_id;
    private List<Item> items = new ArrayList<>();
    private List<Payment> payments = new ArrayList<>();
    private List<Charge> charges = new ArrayList<>();

    public PedidoPagarmeDTO() {}

    public PedidoPagarmeDTO(String customerId, PedidoDTO pedido) {
        this.customer_id = customerId;
        Payment payment = new Payment();
        payment.setAmount((int) Math.round(pedido.getValorTotal() * 100));
        if (TipoPagamentoEnum.CREDITO.equals(pedido.getTipoPagamento())) {
            payment.setPayment_method("credit_card");
            Payment.CreditCard creditCard = new Payment.CreditCard();
            creditCard.setCard_id(pedido.getCartao().getIdPagarme());
            creditCard.setStatement_descriptor("Petalia");
            creditCard.setInstallments(1);
            Payment.CreditCard.Card card = new Payment.CreditCard.Card();
            card.setCvv(pedido.getCartao().getCvv());
            creditCard.setCard(card);
            payment.setCredit_card(creditCard);
        } else if (TipoPagamentoEnum.PIX.equals(pedido.getTipoPagamento())) {
            payment.setPayment_method("Pix");
            Payment.Pix pix = new Payment.Pix();
            pix.setExpires_in(1800);
            payment.setPix(pix);
        }
        getPayments().add(payment);
        Item item = new Item();
        item.setAmount((int) Math.round(pedido.getProduto().getPreco() * 100));
        item.setQuantity(1);
        item.setDescription("Floral Petalia");
        getItems().add(item);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public List<Item> getItems() {
        return items;
    }
    public void setItems(List<Item> items) {
        this.items = items;
    }
    public List<Payment> getPayments() {
        return payments;
    }
    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public List<Charge> getCharges() {
        return charges;
    }

    public void setCharges(List<Charge> charges) {
        this.charges = charges;
    }

    public static class Item {
        private int amount;
        private String description;
        private int quantity;

        // Getters e Setters
        public int getAmount() {
            return amount;
        }
        public void setAmount(int amount) {
            this.amount = amount;
        }
        public String getDescription() {
            return description;
        }
        public void setDescription(String description) {
            this.description = description;
        }
        public int getQuantity() {
            return quantity;
        }
        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }
    }

    public static class Payment {
        private String payment_method;
        private CreditCard credit_card;
        private Pix pix;
        private int amount;

        // Getters e Setters
        public String getPayment_method() {
            return payment_method;
        }
        public void setPayment_method(String payment_method) {
            this.payment_method = payment_method;
        }
        public CreditCard getCredit_card() {
            return credit_card;
        }
        public void setCredit_card(CreditCard credit_card) {
            this.credit_card = credit_card;
        }

        public Pix getPix() {
            return pix;
        }

        public void setPix(Pix pix) {
            this.pix = pix;
        }

        public int getAmount() {
            return amount;
        }
        public void setAmount(int amount) {
            this.amount = amount;
        }

        public static class CreditCard {
            private int installments;
            private String statement_descriptor;
            private String card_id;
            private Card card;

            // Getters e Setters
            public int getInstallments() {
                return installments;
            }
            public void setInstallments(int installments) {
                this.installments = installments;
            }
            public String getStatement_descriptor() {
                return statement_descriptor;
            }
            public void setStatement_descriptor(String statement_descriptor) {
                this.statement_descriptor = statement_descriptor;
            }
            public String getCard_id() {
                return card_id;
            }
            public void setCard_id(String card_id) {
                this.card_id = card_id;
            }
            public Card getCard() {
                return card;
            }
            public void setCard(Card card) {
                this.card = card;
            }

            public static class Card {
                private String cvv;

                // Getters e Setters
                public String getCvv() {
                    return cvv;
                }
                public void setCvv(String cvv) {
                    this.cvv = cvv;
                }
            }
        }

        public static class Pix {
            private int expires_in;

            public int getExpires_in() {
                return expires_in;
            }

            public void setExpires_in(int expires_in) {
                this.expires_in = expires_in;
            }
        }
    }

    public static class Charge {
        private String id;
        private String code;
        private String gatewayId;
        private int amount;
        private String status;
        private String currency;
        private String paymentMethod;
        private String createdAt;
        private String updatedAt;
        private LastTransaction lastTransaction;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getGatewayId() {
            return gatewayId;
        }

        public void setGatewayId(String gatewayId) {
            this.gatewayId = gatewayId;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public LastTransaction getLastTransaction() {
            return lastTransaction;
        }

        public void setLastTransaction(LastTransaction lastTransaction) {
            this.lastTransaction = lastTransaction;
        }

        public static class LastTransaction {
            private String id;
            private String transactionType;
            private String gatewayId;
            private int amount;
            private String status;
            private boolean success;
            private String qrCode;
            private String qrCodeUrl;
            private List<AdditionalInformation> additionalInformation = new ArrayList<>();
            private String expiresAt;
            private String createdAt;
            private String updatedAt;
            private GatewayResponse gatewayResponse;
            private AntifraudResponse antifraudResponse;
            private Metadata metadata;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTransactionType() {
                return transactionType;
            }

            public void setTransactionType(String transactionType) {
                this.transactionType = transactionType;
            }

            public String getGatewayId() {
                return gatewayId;
            }

            public void setGatewayId(String gatewayId) {
                this.gatewayId = gatewayId;
            }

            public int getAmount() {
                return amount;
            }

            public void setAmount(int amount) {
                this.amount = amount;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public boolean isSuccess() {
                return success;
            }

            public void setSuccess(boolean success) {
                this.success = success;
            }

            public String getQrCode() {
                return qrCode;
            }

            public void setQrCode(String qrCode) {
                this.qrCode = qrCode;
            }

            public String getQrCodeUrl() {
                return qrCodeUrl;
            }

            public void setQrCodeUrl(String qrCodeUrl) {
                this.qrCodeUrl = qrCodeUrl;
            }

            public List<AdditionalInformation> getAdditionalInformation() {
                return additionalInformation;
            }

            public void setAdditionalInformation(List<AdditionalInformation> additionalInformation) {
                this.additionalInformation = additionalInformation;
            }

            public String getExpiresAt() {
                return expiresAt;
            }

            public void setExpiresAt(String expiresAt) {
                this.expiresAt = expiresAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public GatewayResponse getGatewayResponse() {
                return gatewayResponse;
            }

            public void setGatewayResponse(GatewayResponse gatewayResponse) {
                this.gatewayResponse = gatewayResponse;
            }

            public AntifraudResponse getAntifraudResponse() {
                return antifraudResponse;
            }

            public void setAntifraudResponse(AntifraudResponse antifraudResponse) {
                this.antifraudResponse = antifraudResponse;
            }

            public Metadata getMetadata() {
                return metadata;
            }

            public void setMetadata(Metadata metadata) {
                this.metadata = metadata;
            }

            public static class AdditionalInformation {
                private String name;
                private String value;

                // Getters and Setters
            }

            public static class GatewayResponse {
                // Assuming there are attributes for gateway response, otherwise leave it empty
            }

            public static class AntifraudResponse {
                // Assuming there are attributes for antifraud response, otherwise leave it empty
            }

            public static class Metadata {
                // Assuming there are attributes for metadata, otherwise leave it empty
            }
        }
    }
}
