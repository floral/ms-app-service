package br.com.frwkapp.pagarme.dto;

public class ClientePagarmeDTO {

    private String id;
    private String name;
    private String email;
    private String code;
    private String document;
    private String type;
    private String document_type;
    private String gender;
    private Address address;
    private String birthdate;
    private Phones phones;
    private Metadata metadata;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getDocument() {
        return document;
    }
    public void setDocument(String document) {
        this.document = document;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getDocument_type() {
        return document_type;
    }
    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }
    public String getBirthdate() {
        return birthdate;
    }
    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
    public Phones getPhones() {
        return phones;
    }
    public void setPhones(Phones phones) {
        this.phones = phones;
    }
    public Metadata getMetadata() {
        return metadata;
    }
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public static class Address {
        private String line_1;
        private String line_2;
        private String zip_code;
        private String city;
        private String state;
        private String country;

        // Getters e Setters
        public String getLine_1() {
            return line_1;
        }
        public void setLine_1(String line_1) {
            this.line_1 = line_1;
        }
        public String getLine_2() {
            return line_2;
        }
        public void setLine_2(String line_2) {
            this.line_2 = line_2;
        }
        public String getZip_code() {
            return zip_code;
        }
        public void setZip_code(String zip_code) {
            this.zip_code = zip_code;
        }
        public String getCity() {
            return city;
        }
        public void setCity(String city) {
            this.city = city;
        }
        public String getState() {
            return state;
        }
        public void setState(String state) {
            this.state = state;
        }
        public String getCountry() {
            return country;
        }
        public void setCountry(String country) {
            this.country = country;
        }
    }

    public static class Phones {
        private Phone home_phone;
        private Phone mobile_phone;

        // Getters e Setters
        public Phone getHome_phone() {
            return home_phone;
        }
        public void setHome_phone(Phone home_phone) {
            this.home_phone = home_phone;
        }
        public Phone getMobile_phone() {
            return mobile_phone;
        }
        public void setMobile_phone(Phone mobile_phone) {
            this.mobile_phone = mobile_phone;
        }

        public static class Phone {
            private String country_code;
            private String area_code;
            private String number;

            // Getters e Setters
            public String getCountry_code() {
                return country_code;
            }
            public void setCountry_code(String country_code) {
                this.country_code = country_code;
            }
            public String getArea_code() {
                return area_code;
            }
            public void setArea_code(String area_code) {
                this.area_code = area_code;
            }
            public String getNumber() {
                return number;
            }
            public void setNumber(String number) {
                this.number = number;
            }
        }
    }

    public static class Metadata {
        private String company;

        // Getters e Setters
        public String getCompany() {
            return company;
        }
        public void setCompany(String company) {
            this.company = company;
        }
    }
}
