package br.com.frwkapp.pagarme.dto;

public class CartaoPagarmeDTO {

    private String id;
    private String billing_address_id;
    private BillingAddress billing_address;
    private String number;
    private String holder_name;
    private String holder_document;
    private int exp_month;
    private int exp_year;
    private String cvv;
    private String brand;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBilling_address_id() {
        return billing_address_id;
    }

    public void setBilling_address_id(String billing_address_id) {
        this.billing_address_id = billing_address_id;
    }

    public BillingAddress getBilling_address() {
        return billing_address;
    }
    public void setBilling_address(BillingAddress billing_address) {
        this.billing_address = billing_address;
    }
    public String getNumber() {
        return number;
    }
    public void setNumber(String number) {
        this.number = number;
    }
    public String getHolder_name() {
        return holder_name;
    }
    public void setHolder_name(String holder_name) {
        this.holder_name = holder_name;
    }
    public String getHolder_document() {
        return holder_document;
    }
    public void setHolder_document(String holder_document) {
        this.holder_document = holder_document;
    }
    public int getExp_month() {
        return exp_month;
    }
    public void setExp_month(int exp_month) {
        this.exp_month = exp_month;
    }
    public int getExp_year() {
        return exp_year;
    }
    public void setExp_year(int exp_year) {
        this.exp_year = exp_year;
    }
    public String getCvv() {
        return cvv;
    }
    public void setCvv(String cvv) {
        this.cvv = cvv;
    }
    public String getBrand() {
        return brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public static class BillingAddress {
        private String line_1;
        private String line_2;
        private String zip_code;
        private String city;
        private String state;
        private String country;

        // Getters e Setters
        public String getLine_1() {
            return line_1;
        }
        public void setLine_1(String line_1) {
            this.line_1 = line_1;
        }
        public String getLine_2() {
            return line_2;
        }
        public void setLine_2(String line_2) {
            this.line_2 = line_2;
        }
        public String getZip_code() {
            return zip_code;
        }
        public void setZip_code(String zip_code) {
            this.zip_code = zip_code;
        }
        public String getCity() {
            return city;
        }
        public void setCity(String city) {
            this.city = city;
        }
        public String getState() {
            return state;
        }
        public void setState(String state) {
            this.state = state;
        }
        public String getCountry() {
            return country;
        }
        public void setCountry(String country) {
            this.country = country;
        }
    }
}
