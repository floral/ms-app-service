package br.com.frwkapp.configuration;

import okhttp3.*;
import okio.Buffer;

import java.io.IOException;

public class LoggingInterceptor implements Interceptor {
    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        RequestBody requestBody = request.body();
        Buffer buffer = new Buffer();
        if (requestBody != null) {
            requestBody.writeTo(buffer);
        }

        System.out.println("Sending request to URL: " + request.url());
//        System.out.println("Request body: " + buffer.readUtf8());

        Response response = chain.proceed(request);

        ResponseBody responseBody = response.body();
        String responseBodyString = responseBody.string();

//        System.out.println("Received response for URL: " + response.request().url());
//        System.out.println("Response body: " + responseBodyString);

        Response newResponse = response.newBuilder()
                .body(ResponseBody.create(responseBody.contentType(), responseBodyString))
                .build();

        return newResponse;
    }
}
